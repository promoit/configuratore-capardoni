PATH=$HOME/tools:$PATH

export PATH

alias php="/opt/plesk/php/7.4/bin/php -c $HOME/tools/php.ini"
alias composer="php $HOME/tools/composer.phar"
alias artisan="php artisan"

cd $HOME/project