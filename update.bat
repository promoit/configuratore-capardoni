@ECHO OFF
git status
PAUSE
git add .
git status
SET /P MESSAGE=Please enter the commit message: 
IF "%MESSAGE%"=="" GOTO Error
git commit -m "%MESSAGE%"
git push origin laravel
ECHO Done!
GOTO End
:Error
ECHO You did not enter the message!
:End
