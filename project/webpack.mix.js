const mix = require('laravel-mix');
const path = require('path');

mix.setPublicPath(path.normalize('../public_html'));

mix.js('resources/js/backend/backend.js', 'backend/js');

mix.js('resources/js/frontend/vue.js', 'frontend/js')
    .vue();

// Configurator

mix.copy('resources/frontend/assets/', '../public_html/frontend/assets');

mix.sass('resources/frontend/scss/main.scss', 'frontend/assets/css')
    .options({ processCssUrls: false });

mix.sass('resources/frontend/scss/pdf.scss', 'frontend/assets/css')
    .options({ processCssUrls: false });

mix.js('resources/frontend/js/app.js', 'frontend/assets/js');
