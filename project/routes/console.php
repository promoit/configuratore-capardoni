<?php

use App\Components\ConfiguratorState;
use App\Http\Resources\Configurator\CompatibilityResource;
use App\Http\Resources\Configurator\ProductResource;
use App\Imports\DatabaseImport;
use App\Models\Compatibility;
use App\Models\Product;
use Illuminate\Support\Facades\Artisan;

Artisan::command('test', function () {
    /*
    $categories = ConfiguratorState::config('categories.skincare');

    $existing = [];
    foreach ($categories as $category) {
        $existing = array_merge($existing, $category['types']);
    }

    $keys = array_keys(ConfiguratorState::config('attributes'));

    $missings = array_diff($keys, $existing);

    $results = [];

    foreach ($missings as $missing) {
        $results[] = [
            'name' => $missing,
            'types' => [$missing],
        ];
    }

    var_export($results);

    die();
    
    //*/

    $products = ['SPNBMS29BOCB-24'];
    // $products = ['ALM30N', 'LPNBMS16AG-18', 'LPNBMS16AG-18-OC'];

    $query = Compatibility::with('products');

    foreach ($products as $product) {
        $query->whereHas('products', function ($query) use ($product) {
            $query->where('web_code', '=', $product);
        });
    }

    $compatibilities = $query->get();

    dd($compatibilities->toArray());






    dd($compatibilities->filter(function ($compatibility) {
        return count($compatibility->products) == 2;
    })->map(function ($compatibility) {
        return $compatibility->code;
    })->toArray());


    $compatibility = Compatibility::where('code', '=', 'PAR30-18B_1')->with('products')->get();
    dd($compatibility->toArray());


    $product = Product::where('web_code', '=', 'DAGW-18')
        ->with(['compatibilities' => function ($query) {
            $query->withCount('products');
        }])->first();

    dd($product->toArray());

    /*

    $product = Product::inRandomOrder()->first();
    $array = (new ProductResource($product))->toArray(null);
    var_export($array);

    die();

    $compatibility = Compatibility::find(387);
    $array = (new CompatibilityResource($compatibility))->toArray();
    var_export($array);

    die();


    $compatibility = Compatibility::find(387)->with('products')->first();

    dd($compatibility->toArray());

    $compatibility = Compatibility::has('products', '=', 3)->inRandomOrder()->first();
    $array = (new CompatibilityResource($compatibility))->toArray();
    dd($array);

    $result = ConfiguratorState::make()->state(Compatibility::WORLD_SKINCARE);
    dd($result);

    //*/
});

/*

Artisan::command('test:import', function () {
    $import = new DatabaseImport();
    $import->import('import.xlsx', 'local');

    foreach ($import->failures() as $sheet => $failures) {
        foreach ($failures as $failure) {
            foreach ($failure->errors() as $error) {
                printf("%s\r\n", $error);
            }
        }
    }
});

//*/