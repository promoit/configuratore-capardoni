<?php

use App\Http\Controllers\Backend\Admin\LeadsController;
use Illuminate\Support\Facades\Route;

Route::get('/', [App\Http\Controllers\Frontend\ConfiguratorController::class, 'index'])->name('frontend.configurator.index');

Route::get('/{world?}', [App\Http\Controllers\Frontend\ConfiguratorController::class, 'show'])->name('frontend.configurator.show')->where(['world' => implode('|', App\Models\Compatibility::WORLDS)]);
Route::post('/store', [App\Http\Controllers\Frontend\ConfiguratorController::class, 'store'])->name('frontend.configurator.store');

// Auth
Route::get('backend/login', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
Route::post('backend/login', [App\Http\Controllers\Auth\LoginController::class, 'login']);
Route::get('backend/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

Route::get('backend/password/reset', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
Route::post('backend/password/email', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
Route::get('backend/password/reset/{token}', [App\Http\Controllers\Auth\ResetPasswordController::class, 'showResetForm'])->name('password.reset');
Route::post('backend/password/reset', [App\Http\Controllers\Auth\ResetPasswordController::class, 'reset'])->name('password.update');
// /Auth

Route::middleware('auth')->group(function () {

    Route::get('/backend/dashboard', [App\Http\Controllers\Backend\DashboardController::class, 'index'])->name('backend.dashboard');

    Route::get('/backend/me/edit', [App\Http\Controllers\Backend\MeController::class, 'edit'])->name('backend.me.edit');
    Route::patch('/backend/me', [App\Http\Controllers\Backend\MeController::class, 'update'])->name('backend.me.update');
    Route::get('/backend/me/depersonate', [App\Http\Controllers\Backend\MeController::class, 'depersonate'])->name('backend.me.depersonate');

    // Admin
    Route::get('/backend/admin/users', [App\Http\Controllers\Backend\Admin\UserController::class, 'index'])->name('backend.admin.users.index');
    Route::get('/backend/admin/users/create', [App\Http\Controllers\Backend\Admin\UserController::class, 'create'])->name('backend.admin.users.create');
    Route::post('/backend/admin/users', [App\Http\Controllers\Backend\Admin\UserController::class, 'store'])->name('backend.admin.users.store');
    Route::get('/backend/admin/users/{user}', [App\Http\Controllers\Backend\Admin\UserController::class, 'show'])->name('backend.admin.users.show');
    Route::get('/backend/admin/users/{user}/edit', [App\Http\Controllers\Backend\Admin\UserController::class, 'edit'])->name('backend.admin.users.edit');
    Route::get('/backend/admin/users/{user}/clone', [App\Http\Controllers\Backend\Admin\UserController::class, 'clone'])->name('backend.admin.users.clone');
    Route::patch('/backend/admin/users/{user}', [App\Http\Controllers\Backend\Admin\UserController::class, 'update'])->name('backend.admin.users.update');
    Route::delete('/backend/admin/users/{user}', [App\Http\Controllers\Backend\Admin\UserController::class, 'delete'])->name('backend.admin.users.delete');

    Route::get('/backend/admin/users/{user}/impersonate', [App\Http\Controllers\Backend\Admin\ImpersonateController::class, 'impersonate'])->name('backend.admin.users.impersonate');

    Route::get('/backend/admin/products', [App\Http\Controllers\Backend\Admin\ProductController::class, 'edit'])->name('backend.admin.products.edit');
    Route::patch('/backend/admin/products', [App\Http\Controllers\Backend\Admin\ProductController::class, 'update'])->name('backend.admin.products.update');

    Route::get('/backend/admin/configurator', [App\Http\Controllers\Backend\Admin\ConfiguratorController::class, 'edit'])->name('backend.admin.configurator.edit');
    Route::patch('/backend/admin/configurator', [App\Http\Controllers\Backend\Admin\ConfiguratorController::class, 'update'])->name('backend.admin.configurator.update');
    // /Admin
    Route::get('backend/admin/{lead}', [LeadsController::class, 'index'])->name('backend.admin.leads');
});
