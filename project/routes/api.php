<?php

use Illuminate\Support\Facades\Route;

Route::post('configurator', [App\Http\Controllers\Api\ConfiguratorController::class, 'index'])->name('api.configurator.index');
Route::post('configurator/products', [App\Http\Controllers\Api\ConfiguratorController::class, 'products'])->name('api.configurator.products');
