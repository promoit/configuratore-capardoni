<?php

namespace App\Models;

use App\Components\ConfiguratorState;
use App\Models\Support\Product as ProductModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends ProductModel
{
    public const IMAGE_EXTENSION = 'png';
    public const IMAGE_PATH_TEMPLATE = 'products/thumbnail/%s.%s';
    public const IMAGE_PATH_PREVIEW_TEMPLATE = 'products/preview/%s.%s';
    public const IMAGE_DEFAULT_PATH_TEMPLATE = 'products/%s.%s';
    public const DEFAULT_IMAGE_PATH_TEMPLATE = 'empty.png';

    protected function getAllAttributesAttribute()
    {
        $attributes = $this->mapped_attributes;

        return array_merge([
            [
                'name' => 'HEIGHT mm',
                'value' => $this->height_formatted
            ],
            [
                'name' => 'DIAMETER mm',
                'value' => $this->length_formatted
            ],
            [
                'name' => 'LENGTH',
                'value' => $this->width_formatted
            ],
            [
                'name' => 'WIDTH',
                'value' => $this->diameter_formatted
            ],
            [
                'name' => 'SUGGESTED LPS mm',
                'value' => $this->suggested_lps_formatted
            ],
            [
                'name' => 'WEIGHT gr',
                'value' => $this->weight_formatted
            ],
            [
                'name' => 'DOSAGE cc',
                'value' => $this->dosage_formatted
            ],
            [
                'name' => 'GREEN OPTION',
                'value' => $this->green_option_formatted
            ],
        ], $attributes);
    }

    protected function getMappedAttributesAttribute()
    {
        $labels = ConfiguratorState::config('attributes.' . $this->type, []);

        $results = [];

        foreach ($labels as $field => $label) {
            $results[] = [
                'name' => $label,
                'value' => $this->$field,
            ];
        }

        return $results;
    }

    protected function getImageUrlAttribute()
    {
        $path = $this->getImageFolderPath($this->image);

        return Storage::disk('public')->exists($path) ? Storage::disk('public')->url($path) : Storage::disk('public')->url(static::DEFAULT_IMAGE_PATH_TEMPLATE);
    }

    protected function getImagePathAttribute()
    {
        $path = $this->getImageFolderPath($this->image);

        return Storage::disk('public')->path($path);
    }

    protected function getImageFullUrlAttribute()
    {
        $path = $this->getImageFolderPath($this->image, true);

        return Storage::disk('public')->exists($path) ? Storage::disk('public')->url($path) : Storage::disk('public')->url(static::DEFAULT_IMAGE_PATH_TEMPLATE);
    }

    protected function getImageFullPathAttribute()
    {
        $path = $this->getImageFolderPath($this->image, true);

        return Storage::disk('public')->path($path);
    }

    protected function getImagesAttribute()
    {
        return static::getImages($this->image);
    }

    /* protected function getImageAlignmentAttribute()
    {
        $img = $this->alignment()->first();
        $img = ImgAlignment::select('w', 'h', 'yt', 'yb')
        ->where('img', 'LIKE', "{$this->image}.%")
        ->first();
        return $img;
    } */

    public static function getImages($image)
    {
        $files = \File::glob(storage_path('app/public/products') . "/{$image}_[a-zA-Z]*.png");

        if (!empty($files)) {
            $files = array_map(function ($file) {
                $path = static::getImageFolderPath(pathinfo($file, PATHINFO_FILENAME), true);

                return Storage::disk('public')->exists($path)
                    ? Storage::disk('public')->url($path)
                    : Storage::disk('public')->url(static::DEFAULT_IMAGE_PATH_TEMPLATE);
            }, $files);
        }

        return $files;
    }

    public static function getImageFolderPath($image, $useFull = false)
    {
        if ($useFull) {
            return sprintf(static::IMAGE_PATH_TEMPLATE, $image, static::IMAGE_EXTENSION);
        }

        $path = sprintf(static::IMAGE_PATH_PREVIEW_TEMPLATE, $image, static::IMAGE_EXTENSION);

        if (!Storage::disk('public')->exists($path)) {
            $path = sprintf(static::IMAGE_DEFAULT_PATH_TEMPLATE, $image, static::IMAGE_EXTENSION);
        }

        return $path;
    }

    public function __get($name)
    {
        if (Str::endsWith($name, '_formatted')) {
            $name = preg_replace('#_formatted$#si', '', $name);
            return (is_null($this->$name) || ($this->$name === '')) ? null : str_replace('.', ',', $this->$name);
        }

        return parent::__get($name);
    }

    public function alignment(): HasOne
    {
        return $this->hasOne(ImgAlignment::class,'id','img_alignment_id');
    }
}
