<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImgAlignment extends Model
{
    use HasFactory;
    protected $table = 'img_alignment';
    protected $fillable = ['img','checksum','w','h','yt','yb'];
    public $timestamps = false;
}
