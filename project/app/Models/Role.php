<?php

namespace App\Models;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    public const ADMIN = 'admin';

    protected $fillable = [
        'name', 'display_name', 'description',
    ];
}
