<?php

namespace App\Models;

use App\Models\Support\Lead as LeadModel;

class Lead extends LeadModel
{
    public const TYPE_QUOTE = 'quote';
    public const TYPE_SAVE = 'save';
    public const TYPE_DOWNLOAD = 'download';
}
