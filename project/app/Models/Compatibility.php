<?php

namespace App\Models;

use App\Models\Support\Compatibility as CompatibilityModel;
use Illuminate\Support\Facades\Storage;

class Compatibility extends CompatibilityModel
{
    //  Must match configurator/utils/constants.js
    public const WORLD_SKINCARE = 'make-up-skincare';
    public const WORLD_PERSONAL_CARE = 'personal-care';
    public const WORLD_NAIL_POLISH = 'nail-polish';
    // public const WORLD_MAKE_UP = 'make-up';
    public const WORLD_PERFUMERY = 'perfumery';
    public const WORLD_HOME_FRAGRANCES = 'home-fragrances';
    public const WORLD_CAPS_FOR_ALU_TUBES = 'caps-for-alu-tubes';

    public const WORLDS = [
        self::WORLD_SKINCARE,
        self::WORLD_PERSONAL_CARE,
        self::WORLD_NAIL_POLISH,
        // self::WORLD_MAKE_UP,
        self::WORLD_PERFUMERY,
        self::WORLD_HOME_FRAGRANCES,
        self::WORLD_CAPS_FOR_ALU_TUBES,
    ];

    protected const WORLD_MAPPING = [
        self::WORLD_SKINCARE => 'skincare',
        self::WORLD_PERSONAL_CARE => 'personal_care',
        self::WORLD_NAIL_POLISH => 'nail_polish',
        // self::WORLD_MAKE_UP => 'make_up',
        self::WORLD_PERFUMERY => 'perfumery',
        self::WORLD_HOME_FRAGRANCES => 'home_fragrances',
        self::WORLD_CAPS_FOR_ALU_TUBES => 'caps_for_alu_tubes',
    ];

    public function scopeWhereWorld($query, $world)
    {
        $query->where(static::WORLD_MAPPING[$world], '=', true);
    }

    protected function getImageUrlAttribute()
    {
        $path = Product::getImageFolderPath($this->image, true);
        return Storage::disk('public')->url($path);
    }

    protected function getImagePathAttribute()
    {
        $path = Product::getImageFolderPath($this->image, true);
        return Storage::disk('public')->path($path);
    }

    protected function getImagesAttribute()
    {
        return Product::getImages($this->image);
    }
}
