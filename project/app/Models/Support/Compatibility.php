<?php

namespace App\Models\Support;

use App\Models\Product;

class Compatibility extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var  string
     */
    protected $table = 'compatibilities';

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'skincare', 'personal_care', 'nail_polish', 'make_up', 'perfumery', 'home_fragrances', 'title', 'code', 'image',
    ];

    /**
     * The model's attributes.
     *
     * @var  array
     */
    protected $attributes = [
        'skincare' => false,
        'personal_care' => false,
        'nail_polish' => false,
        'make_up' => false,
        'perfumery' => false,
        'home_fragrances' => false,
        'title' => '',
        'code' => '',
        'image' => '',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var  array
     */
    protected $casts = [
        'skincare' => 'boolean',
        'personal_care' => 'boolean',
        'nail_polish' => 'boolean',
        'make_up' => 'boolean',
        'perfumery' => 'boolean',
        'home_fragrances' => 'boolean',
    ];

    /**
     * The records that belong to this model.
     */
    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class, 'product_compatibility', 'compatibility_id', 'product_id')
            ->withTimestamps()
            ->withPivot('type');
    }
}
