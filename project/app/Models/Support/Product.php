<?php

namespace App\Models\Support;

use App\Models\Compatibility;

class Product extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var  string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'type', 'image', 'capardoni_code', 'web_code', 'name', 'height', 'length', 'width', 'diameter', 'suggested_lps', 'weight', 'dosage', 'green_option', 'attrib_1', 'attrib_2', 'attrib_3', 'attrib_4', 'attrib_5', 'attrib_6', 'attrib_7', 'attrib_8',
    ];

    /**
     * The model's attributes.
     *
     * @var  array
     */
    protected $attributes = [
        'type' => '',
        'image' => '',
        'capardoni_code' => NULL,
        'web_code' => '',
        'name' => '',
        'height' => NULL,
        'length' => NULL,
        'width' => NULL,
        'diameter' => NULL,
        'suggested_lps' => NULL,
        'weight' => NULL,
        'dosage' => NULL,
        'green_option' => NULL,
        'attrib_1' => NULL,
        'attrib_2' => NULL,
        'attrib_3' => NULL,
        'attrib_4' => NULL,
        'attrib_5' => NULL,
        'attrib_6' => NULL,
        'attrib_7' => NULL,
        'attrib_8' => NULL,
    ];

    public function getSuggestedLpsAttribute($value)
    {
        if (!$value) {
            return $value;
        }

        if (floatval($value) === floatval(intval($value))) {
            $value = intval($value);
        }

        return "{$value}mm";
    }

    /**
     * The records that belong to this model.
     */
    public function compatibilities()
    {
        return $this->belongsToMany(\App\Models\Compatibility::class, 'product_compatibility', 'product_id', 'compatibility_id')
            ->withTimestamps()
            ->withPivot('type');
    }
}
