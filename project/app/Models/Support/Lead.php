<?php

namespace App\Models\Support;


class Lead extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var  string
     */
    protected $table = 'leads';

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'url', 'world', 'type', 'firstname', 'lastname', 'company', 'phone', 'country', 'email', 'message', 'privacy_at', 'marketing_at',
    ];

    /**
     * The model's attributes.
     *
     * @var  array
     */
    protected $attributes = [
        'url' => '',
        'world' => '',
        'type' => '',
        'firstname' => '',
        'lastname' => '',
        'company' => NULL,
        'phone' => NULL,
        'country' => '',
        'email' => '',
        'message' => NULL,
        'privacy_at' => NULL,
        'marketing_at' => NULL,
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var  array
     */
    protected $dates = [
        'privacy_at', 'marketing_at',
    ];
}
