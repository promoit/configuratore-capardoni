<?php

namespace App\Http\Controllers\Backend;

use FilippoToso\LaravelHelpers\Facades\Breadcrumbs;
use App\Http\Controllers\BackendController;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class DashboardController extends BackendController
{
    protected $route = 'backend.dashboard';

    public function index(Request $request)
    {
        Breadcrumbs::set([
            route('backend.dashboard') => 'Dashboard',
        ]);

        return view('backend.dashboard');
    }
}
