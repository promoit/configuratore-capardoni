<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\Controller;
use App\Models\Lead;
use FilippoToso\LaravelHelpers\Facades\Breadcrumbs;
use Illuminate\Http\Request;

class LeadsController extends AdminController
{
    protected $route = 'backend.admin.leads';
    public function index($page)
    {
        Breadcrumbs::set([
            route('backend.dashboard') => 'Dashboard',
            route($this->route, $page) => 'Lead ' . $page,
        ]);
        $leads = Lead::where('type', '=', $page)->get();

        switch ($page) {
            case 'quote':
                $page = 'Configurazione';
                break;
            case 'save':
                $page = 'Salvataggio';
                break;
            case 'download':
                $page = 'Configurazione';
                break;

            default:
                $page;
                break;
        }
        return view('backend.leads.index', ['leads' => $leads, 'page' => $page]);
    }
}
