<?php

namespace App\Http\Controllers\Backend\Admin;

use Throwable;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Compatibility;
use App\Imports\DatabaseImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Cache;
use FilippoToso\LaravelHelpers\Facades\Breadcrumbs;

class ProductController extends AdminController
{
    protected $route = 'backend.admin.products';

    public function edit(Request $request)
    {
        Breadcrumbs::set([
            route('backend.dashboard') => 'Dashboard',
            route($this->route . '.edit') => 'Importazione Prodotti',
        ]);

        return view($this->view . '.edit');
    }

    public function update(Request $request)
    {
        Breadcrumbs::set([
            route('backend.dashboard') => 'Dashboard',
            route($this->route . '.edit') => 'Importazione Prodotti',
            route($this->route . '.update') => 'Importazione Completata',
        ]);

        $data = $request->validate([
            'file' => 'required|file|extensions:xlsx',
        ]);

        $importer = new DatabaseImport();

        try {
            ini_set('max_execution_time', 0);
            ini_set('memory_limit', '-1');
            Excel::import($importer, $data['file'], null, \Maatwebsite\Excel\Excel::XLSX);
        } catch (Throwable $th) {
            report($th);
            flash("E' avvenuto un errore nell'importazione! Prova a copiare e incollare i valori di tutti i fogli del file Excel e riprovare.")->error();
            flash($th->getMessage())->error();
            return redirect()->back()->withInput();
        }

        Cache::flush();

        return view($this->view . '.show', [
            'productCodes' => $this->productCodes($importer),
            'images' => $this->images(),
        ]);
    }

    protected function images()
    {
        return Product::select('image')
            ->union(Compatibility::select('image'))
            ->get()
            ->map(function ($product) {
                return $product->image_path;
            })->filter(function ($file) {
                return !file_exists($file);
            })->map(function ($file) {
                return basename($file, '.' . Product::IMAGE_EXTENSION);
            });
    }

    protected function productCodes($importer)
    {
        $productCodes = [];

        foreach ($importer->failures() as $sheet => $failures) {
            foreach ($failures as $failure) {
                foreach ($failure->errors() as $error) {
                    $productCodes[] = $error;
                }
            }
        }

        $productCodes = array_unique($productCodes);

        sort($productCodes);

        return $productCodes;
    }
}
