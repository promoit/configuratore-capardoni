<?php

namespace App\Http\Controllers\Backend\Admin;

use FilippoToso\LaravelHelpers\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Mails\UserCreated;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Role;

class UserController extends AdminController
{
    protected $route = 'backend.admin.users';

    /**
     * Display a listing of the resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Breadcrumbs::set([
            route('backend.dashboard') => 'Dashboard',
            route($this->route . '.index') => 'Utenti',
        ]);

        $input = $request->only('search');

        $query = User::orderBy('name');

        if ($request->filled('search')) {
            $query->search($request->input('search'));
        }

        $users = $query->paginate()->appends($input);

        return view($this->view . '.index', [
            'users' => $users,
            'input' => $input,
            'roles' => $this->getRoles(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = new User;

        Breadcrumbs::set([
            route('backend.dashboard') => 'Dashboard',
            route($this->route . '.index') => 'Utenti',
            route($this->route . '.create', ['user' => $user]) => 'Utente',
        ]);

        return view($this->view . '.create', [
            'user' => $user,
            'roles' => $this->getRoles(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|string|not_exists:users,email',
            'password' => 'required|confirmed',
            'roles.*' => 'nullable|exists:roles,name',
        ];

        $data = $request->validate($rules);

        $password = $data['password'];

        $data = $this->setPassword($data);

        $data['email_verified_at'] = Carbon::now();

        $user = User::create($data);

        if ($request->filled('notify')) {
            Mail::to($user->email, $user->name)->queue(new UserCreated($user, $password));
        }

        $data['roles'] = $data['roles'] ?? [];
        $user->syncRoles($data['roles']);
        unset($data['roles']);

        flash('Utente creato con successo!')->success();
        return redirect()->route('backend.admin.users.index');
    }

    /**
     * Clone a resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function clone(Request $request, User $user)
    {
        Breadcrumbs::set([
            route('backend.dashboard') => 'Dashboard',
            route($this->route . '.index') => 'Utenti',
            route($this->route . '.clone', ['user' => $user]) => 'Clona Utente',
        ]);

        return view($this->view . '.create', [
            'user' => $user,
            'roles' => $this->getRoles(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    User $user
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
        return view($this->view . '.show', [
            'user' => $user,
            'roles' => $this->getRoles(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    User $user
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, User $user)
    {
        Breadcrumbs::set([
            route('backend.dashboard') => 'Dashboard',
            route($this->route . '.index') => 'Utenti',
            route($this->route . '.edit', ['user' => $user]) => 'Modifica Utente',
        ]);

        return view($this->view . '.edit', [
            'user' => $user,
            'roles' => $this->getRoles(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    User $user
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|string',
            'password' => 'nullable|confirmed',
            'roles.*' => 'nullable|exists:roles,name',
        ];

        $data = $request->validate($rules);
        $data = $this->setPassword($data);

        if ($user->email != $data['email']) {
            $existing = User::where('email', '=', $data['email'])->first();
            if ($existing) {
                unset($data['email']);
                $user->update($data);

                flash("L'indirizzo email inserito è già in uso!", 'danger');
                return redirect()->route('backend.admin.users.edit', ['user' => $user])->withInput();
            }
        }

        $data['roles'] = $data['roles'] ?? [];
        $user->syncRoles($data['roles']);
        unset($data['roles']);

        $user->update($data);

        flash('Utente aggiornato con successo!')->success();

        return redirect()->route('backend.admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    User $user
     * @return  \Illuminate\Http\Response
     */
    public function delete(Request $request, User $user)
    {
        $user->delete();

        flash('Utente eliminato con successo!')->success();
        return redirect()->back()->withInput();
    }

    protected function setPassword($data)
    {
        if (array_key_exists('password', $data)) {
            if (is_null($data['password'])) {
                unset($data['password']);
            } else {
                $data['password'] = Hash::make($data['password']);
            }
        }
        return $data;
    }

    protected function getRoles()
    {
        return Role::pluck('display_name', 'name');
    }
}
