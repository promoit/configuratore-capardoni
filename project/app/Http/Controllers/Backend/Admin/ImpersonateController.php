<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\User;

class ImpersonateController extends AdminController
{

    /**
     * Impersonate a user
     * @param  Request $request [description]
     * @param  User    $user    [description]
     * @return [type]           [description]
     */
    public function impersonate(Request $request, User $user)
    {
        if ($request->user()->hasRole(Role::ADMIN)) {
            $request->user()->impersonate($user->id);
        }

        return redirect()->route('backend.dashboard');
    }
}
