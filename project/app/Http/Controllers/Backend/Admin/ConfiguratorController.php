<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Models\Product;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use FilippoToso\LaravelHelpers\Facades\Breadcrumbs;

class ConfiguratorController extends AdminController
{
    protected $route = 'backend.admin.configurator';

    protected const FILE = 'configurator.php';
    protected const TEMP = 'configurator-temp.php';

    public function edit(Request $request)
    {
        Breadcrumbs::set([
            route('backend.dashboard') => 'Dashboard',
            route($this->route . '.edit') => 'Configurazione',
        ]);

        return view($this->view . '.edit', [
            'content' => Storage::disk('local')->get(static::FILE),
        ]);
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'content' => 'required|string',
        ]);

        if (!$this->checkSyntax($data['content'])) {
            flash('La configurazione caricata contiene un errore!')->error();
            return redirect()->back()->withInput();
        }

        Cache::flush();

        Storage::disk('local')->put(static::FILE, $data['content']);

        flash('Configurazione salvata con successo!')->success();

        $this->checkConfiguration();

        return redirect()->route($this->route . '.edit');
    }

    protected function checkConfiguration()
    {
        $config = include(storage_path('app/' . static::FILE));

        $productTypes = Product::distinct()->pluck('type')->toArray();

        $types = array_keys($config['attributes'] ?? []);
        $missingTypes = array_diff($productTypes, $types);
        if (!empty($missingTypes)) {
            flash(sprintf('Nel campo attributes manca la configurazione per i seguenti type: %s!', implode(', ', $missingTypes)))->warning();
        }

        $types = array_keys($config['filters'] ?? []);
        $missingTypes = array_diff($productTypes, $types);
        if (!empty($missingTypes)) {
            flash(sprintf('Nel campo filters manca la configurazione per i seguenti type: %s!', implode(', ', $missingTypes)))->warning();
        }
    }

    protected function checkSyntax($content)
    {
        Storage::disk('local')->put(static::TEMP, $content);

        $config = null;

        try {
            $config = include(storage_path('app/' . static::TEMP));
        } catch (\Throwable $th) {
        }

        Storage::disk('local')->delete(static::TEMP);

        return is_array($config);
    }
}
