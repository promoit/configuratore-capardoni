<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\BackendController as Controller;
use App\Models\Role;

class AdminController extends Controller
{
    protected $role = Role::ADMIN;
}
