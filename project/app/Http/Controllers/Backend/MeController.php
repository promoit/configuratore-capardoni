<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController as Controller;
use FilippoToso\LaravelHelpers\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;

class MeController extends Controller
{

    protected $route = 'backend.me';

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        Breadcrumbs::set([
            route('backend.dashboard') => 'Dashboard',
            route('backend.me.edit') => 'Profilo',
        ]);

        return view('backend.me.edit', ['user' => $request->user()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = $request->user();

        $rules = [
            'name' => 'required|string',
            'email' => 'required|string',
            'password' => 'nullable|confirmed',
        ];
        $data = $request->validate($rules);

        $data = $this->setPassword($data);

        if ($user->email != $data['email']) {
            $existing = User::where('email', '=', $data['email'])->first();
            if ($existing) {
                unset($data['email']);
                $user->update($data);

                flash("L'indirizzo email inserito è già in uso!", 'danger');
                return redirect()->route('backend.me.edit')->withInput();
            }
        }

        $user->update($data);

        flash('Prpfilo modificato con successo!')->success();
        return redirect()->route('backend.me.edit');
    }

    protected function setPassword($data)
    {
        if (array_key_exists('password', $data)) {
            if (is_null($data['password'])) {
                unset($data['password']);
            } else {
                $data['password'] = Hash::make($data['password']);
            }
        }
        return $data;
    }

    /**
     * Depersonate a user
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function depersonate(Request $request)
    {

        $request->user()->depersonate();
        return redirect()->route('backend.dashboard');
    }
}
