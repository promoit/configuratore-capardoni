<?php

namespace App\Http\Controllers\Api;

use App\Components\ConfiguratorState;
use App\Http\Resources\Configurator\ProductResource;
use App\Models\Compatibility;
use App\Models\Product;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class ConfiguratorController extends ApiController
{
    public function index(Request $request)
    {
        $data = $request->validate([
            'world' => 'required|in:' . implode(',', Compatibility::WORLDS),
            'filters' => 'array',
        ]);

        $state = ConfiguratorState::make()->state($data['world'], $data['filters']);

        return response()->json($state);
    }

    public function products(Request $request){
        $data = $request->validate([
            'world' => 'required|in:' . implode(',', Compatibility::WORLDS),
        ]);
        $world = $data['world'];

        $cacheKey = "product_for_".$world;

        return Cache::rememberForever($cacheKey,fn()=>Product::with('alignment')->whereHas('compatibilities', function ($query) use ($world) {
            $query->whereWorld($world);
        })->get()
            ->mapWithKeys(function ($product) {
                return [$product->web_code => new ProductResource($product)];
            })
            ->toArray());
    }
}
