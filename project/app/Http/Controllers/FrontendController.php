<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;

class FrontendController extends Controller
{
    protected $route = null;


    public function __construct()
    {
        View::share('route', $this->route);
        View::share('view', $this->view);
    }

    public function __get($name)
    {
        if ($name == 'view') {
            return $this->route;
        }

        return null;
    }
}
