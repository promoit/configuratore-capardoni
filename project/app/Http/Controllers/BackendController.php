<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Exceptions\HttpResponseException;

class BackendController extends Controller
{
    protected $route = null;

    protected $role = null;

    protected $user = null;

    public function __construct()
    {
        View::share('route', $this->route);
        View::share('view', $this->view);

        $this->middleware(function ($request, $next) {

            $this->user = $request->user();

            if ($this->role && !$this->user->hasRole($this->role)) {
                flash('Non puoi accedere a questa funzione!')->error();
                return redirect()->route('backend.dashboard');
            }

            return $next($request);
        });
    }

    public function authorized($ownerId)
    {
        $ownerId = is_a($ownerId, Model::class) ? $ownerId->owner_id : $ownerId;

        if ($this->user->id != $ownerId) {
            flash('Non puoi accedere a questa risorsa!', 'danger');
            $route = Route::has($this->route . '.index') ? $this->route . '.index' : 'backend.dashboard';
            throw new HttpResponseException(redirect()->route($route));
        }

        return true;
    }

    public function __get($name)
    {
        if ($name == 'view') {
            return $this->route;
        }

        return null;
    }
}
