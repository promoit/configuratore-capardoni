<?php

namespace App\Http\Controllers\Frontend;

use DOMElement;
use Carbon\Carbon;
use App\Models\Lead;
use App\Models\Product;
use App\Rules\ReCAPTCHA;
use Illuminate\Http\Request;
use App\Mails\Leads\SaveMail;
use App\Models\Compatibility;
use App\Mails\Leads\QuoteMail;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Classes\AdvDemInterfaceAPI;
use Illuminate\Support\Facades\Mail;
use App\Components\ConfiguratorState;
use App\Http\Controllers\FrontendController;
use App\Http\Resources\Configurator\ProductResource;
use App\Mails\DownloadDetails;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

class ConfiguratorController extends FrontendController
{
    protected $route = 'frontend.configurator';

    public function index()
    {
        $worlds = ConfiguratorState::config('worlds');

        foreach ($worlds as $world => $label) {
            $empty = !Compatibility::whereWorld($world)->exists() || (ConfiguratorState::config('disabled')[$world] ?? false);

            $worlds[$world] = [
                'label' => $label,
                'empty' => $empty
            ];
        }

        return view('frontend.configurator.index', [
            'worlds' => $worlds,
        ]);
    }

    public function show(Request $request, $world = Compatibility::WORLD_SKINCARE)
    {

        $worldFields = ConfiguratorState::config("worlds_fields." . $world);


        return view('frontend.configurator.show', [
            'world' => $world,
            'products' => [],
            /* 'state' => [
                'categories' => [],
                'filters' => [],
                'products' => [],
                'combination' => null,
            ], */
            'state' => $this->firstState($request, $world),
            "world_field_minimum_qty" => $worldFields['minimum_qty'],
        ]);
    }

    public function products()
    {
        return Cache::rememberForever('all_products',fn()=>Product::with('alignment')->get()
            ->mapWithKeys(function ($product) {
                return [$product->web_code => new ProductResource($product)];
            })
            ->toArray());
    }

    protected function state(Request $request, $world)
    {
        $products = $request->input('products', []);
        $loadedFilters = $request->input('filters', []);
        $configurator = ConfiguratorState::make();
        $filters = $configurator->filtersFromProducts($products, $loadedFilters);

        $state = $configurator->state($world, $filters);

        return $state;
    }

    protected function firstState(Request $request, $world)
    {
        $products = $request->input('products', []);
        $loadedFilters = $request->input('filters', []);
        $configurator = ConfiguratorState::make();
        $filters = $configurator->filtersFromProducts($products, $loadedFilters);

        $state = $configurator->firstState($world, $filters);

        return $state;
    }

    public function store(Request $request)
    {
        $action = $request->input('action');
        switch ($action) {
            case 'save':
                return $this->save($request);
                break;
            case 'quote':
                return $this->quote($request);
                break;
            case 'details':
                return $this->download($request);
                break;

            default:
                dd($action);
                break;
        }

        abort(404);
    }
    protected function save(Request $request)
    {
        $data = $request->validate([
            'url' => 'required|string',
            'world' => 'required|string',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'company' => 'nullable|string',
            'email' => 'required|email',
            'country' => 'required|string',
            'privacy' => 'required|accepted',
            'token' => new ReCAPTCHA,
        ]);

        $lead = Lead::create([
            'type' => Lead::TYPE_SAVE,
            'world' => $data['world'],
            'url' => $data['url'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'company' => $data['company'] ?? null,
            'email' => $data['email'],
            'country' => $data['country'],
            'privacy_at' => Carbon::now(),
            'marketing_at' => ($data['privacy'] ?? null) ? Carbon::now() : null,
        ]);

        Mail::to($lead->email)->send(new SaveMail($lead));

        $sent = empty(Mail::failures());

        if (config('project.leads.addresses')) {
            Mail::to(config('project.leads.addresses'))->send(new SaveMail($lead));
        }

        if (config('project.leads.bcc')) {
            Mail::to(config('project.leads.bcc'))->send(new QuoteMail($lead));
        }

        if ($data['privacy']) {
            $newsletter_field = [
                'ip' => $request->ip(),
                'world' => $data['world'],
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'company' => $data['company'] ?? null,
                'country' => $data['country'],
                'email' => $data['email'],
                'action' => __FUNCTION__,
            ];

            $this->subscribeToNewsletter($newsletter_field);
        }

        return response()->json([
            'sent' => $sent,
            'message' => $sent ? config('project.messages.save.success') : config('project.messages.save.failure'),
        ]);
    }

    protected function quote(Request $request)
    {
        $data = $request->validate([
            'url' => 'required|string',
            'world' => 'required|string',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'company' => 'nullable|string',
            'phone' => 'nullable|string',
            'country' => 'required|string',
            'email' => 'required|email',
            'message' => 'nullable|string',
            'privacy' => 'required|accepted',
            'marketing' => 'nullable|boolean',
            'token' => new ReCAPTCHA,
        ]);

        $lead = Lead::create([
            'type' => Lead::TYPE_QUOTE,
            'world' => $data['world'],
            'url' => $data['url'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'company' => $data['company'] ?? null,
            'country' => $data['country'],
            'email' => $data['email'],
            'phone' => $data['phone'] ?? null,
            'message' => $data['message'] ?? null,
            'privacy_at' => Carbon::now(),
            'marketing_at' => ($data['marketing'] ?? null) ? Carbon::now() : null,
        ]);
        // Mail::to($lead->email)->send(new QuoteMail($lead));
        // $sent = empty(Mail::failures());

        if (config('project.leads.addresses')) {
            Mail::to(config('project.leads.addresses'))->send(new QuoteMail($lead));
        }

        if (config('project.leads.bcc')) {
            Mail::to(config('project.leads.bcc'))->send(new QuoteMail($lead));
        }

        $sent = empty(Mail::failures());

        if ($data['marketing']) {
            $newsletter_field = [
                'ip' => $request->ip(),
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'world' => $data['world'],
                'company' => $data['company'] ?? null,
                'country' => $data['country'] ?? null,
                'email' => $data['email'],
                'phone' => $data['phone'] ?? null,
                'action' => __FUNCTION__,
            ];
//Qui funziona
            $this->subscribeToNewsletter($newsletter_field);
        }

        return response()->json([
            'sent' => $sent,
            'message' => $sent ? config('project.messages.quote.success') : config('project.messages.quote.failure'),
        ]);
    }

    protected function download_details(Request $request)
    {
        $products = false;
        $combination = false;
        $world = false;
        $pdf_name = 'details-';

        //dd($request);
        if ($request->has('combination')) {
            $combination = Compatibility::find($request->input('combination'));
            $pdf_name = Str::slug($combination['title']) . '-';
        }

        if ($request->has('world')) {
            $world = $request->input('world');
            $worlds = ConfiguratorState::config("worlds");
            $label = $world;
            if (array_key_exists($world, $worlds)) {
                $label = $worlds[$world];
            }

            $world = [
                'id' => $world,
                'label' => $label,
            ];

            $pdf_name .= $world['id'] . '-';
        }

        if ($request->has('products')) {
            $products = $request->input('products');
            if (!is_array($products)) {
                $products = [$products];
            }

            $categories = ConfiguratorState::config("categories." . $request->input('world'));
            $types = array();
            foreach ($categories as $category) {
                $types = array_merge($types, $category['types']);
            }


            $products = array_map(function ($product) {
                return Product::find($product);
            }, $products);


            foreach ($types as $k => $type) {
                foreach ($products as $product) {
                    if ($product['type'] === $type) {
                        $types[] = $product;
                    }
                }
                if (!is_array($types[$k])) {
                    unset($types[$k]);
                }
            }
        }
        $products = $types;

        $pdf = Pdf::loadView('pdf.details', [
            'combination' => $combination,
            'products' => $products,
            'world' => $world,
        ]);

        $placeholder = [];

        $pdf->setCallbacks([
            [
                'event' => 'end_frame',
                'f' => function ($frame, $canvas) use (&$placeholder) {
                    /** @var DOMElement */
                    $node = $frame->get_node();

                    if ($node instanceof DOMElement) {
                        $class = $node->getAttribute('class');

                        if (!array_key_exists($canvas->get_page_number(), $placeholder)) {
                            $placeholder[$canvas->get_page_number()] = [
                                'category' => null,
                                'world' => null,
                            ];
                        }

                        switch ($class) {
                            case 'pdf-place-category':
                                $placeholder[$canvas->get_page_number()]['category'] = $frame->get_padding_box();
                                break;
                            case 'pdf-place-world':
                                $placeholder[$canvas->get_page_number()]['world'] = $frame->get_padding_box();
                                break;
                        }
                    }
                }
            ],
            [
                'event' => 'end_document',
                'f' => function ($pageNumber, $pageCount, $canvas, $fontMetrics) use (&$placeholder, $world) {
                    $font = $fontMetrics->getFont('Wotfard');
                    $pixel_into_mm = 0.264583;

                    foreach ($placeholder[$pageNumber] as $key => $value) {
                        switch ($key) {
                            case 'category':
                                $fontSize = 21;
                                if ($pageNumber === 1) {
                                    $text = __('pdf.header.category') . " /";
                                } else {
                                    $text = __("pdf.header.page");
                                }
                                break;
                            case 'world':
                                $fontSize = 26;
                                if ($pageNumber === 1) {
                                    $text = strtoupper($world['label']);
                                } else {
                                    $text = __("pdf.header.pagination", [
                                        'number' => $pageNumber,
                                        'count' => $pageCount,
                                    ]);
                                }
                                break;
                        }

                        $fontSize *= $pixel_into_mm;

                        $width = $fontMetrics->getTextWidth($text, $font, $fontSize);
                        $canvas->text($value['x'] + $value['w'] - $width, $value['y'], $text, $font, $fontSize, [0, 30 / 255, 96 / 255]);
                    }
                }
            ],
        ]);

        $path = storage_path() . '/app/pdf/';

        if (!File::exists($path)) {
            File::makeDirectory($path);
        }
        $pdf_name .= Carbon::now()->format('Y-m-d-H-i-s');
        $pdf->save($path . $pdf_name . '.pdf');
        return array('pdf_name' => $pdf_name, 'combination' => $combination);
    }

    protected function download(Request $request)
    {
        $pdf = $this->download_details($request);
        $data = $request->validate([
            'url' => 'required|string',
            'world' => 'required|string',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'company' => 'nullable|string',
            'country' =>'required|string',
            'email' => 'required|email',
            'privacy' => 'required|accepted',
            'token' => new ReCAPTCHA,
        ]);

        Lead::create([
            'type' => Lead::TYPE_DOWNLOAD,
            'world' => $data['world'],
            'url' => $data['url'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'company' => $data['company'] ?? null,
            'country' => $data['country'],
            'email' => $data['email'],
            'phone' => $data['phone'] ?? null,
            'message' => $data['message'] ?? null,
            'privacy_at' => Carbon::now(),
            'marketing_at' => ($data['privacy'] ?? null) ? Carbon::now() : null,
        ]);

        Mail::to($data['email'])->send(new DownloadDetails($pdf));

        if ($data['privacy']) {
            $newsletter_field = [
                'ip' => $request->ip(),
                'world' => $data['world'],
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'company' => $data['company'] ?? null,
                'country' => $data['country'],
                'email' => $data['email'],
                'action' => __FUNCTION__,
            ];

            $this->subscribeToNewsletter($newsletter_field);
        }

        $sent = empty(Mail::failures());
        $file = storage_path() . '/app/pdf/' . $pdf['pdf_name'] . '.pdf';

        if (File::exists($file)) {
            File::delete($file);
        }

        return response()->json([
            'sent' => $sent,
            'message' => $sent ? config('project.messages.download.success') : config('project.messages.download.failure'),
        ]);
    }

    private function subscribeToNewsletter($data)
    {
        if (!config('opidem.apikey') || !config('opidem.entrypoint')) {
            return null;
        }

        $opidem = new AdvDemInterfaceAPI(config('opidem.apikey'), config('opidem.entrypoint'));

        if ($opidem->getRequestSuccessful()) {
            $recipient_id = config('opidem.recipient');
            $email = $data['email'];

            $custom_fields = [
                [
                    'id' => 5664, // Lingua
                    'value' => 'ENG'
                ],
                [
                    'id' => 5917, // Nome
                    'value' => $data['firstname']
                ],
                [
                    'id' => 5918, // Cognome
                    'value' => $data['lastname']
                ],
                [
                    'id' => 5644, // Ragione sociale
                    'value' => $data['company']
                ],
                [
                    'id' => 5680, // Telefono
                    'value' => $data['phone'] ?? null
                ],
                [
                    'id' => 5645, // Paese
                    'value' => $data['country'] ?? null
                ],
                [
                    'id' => 5665, // Form
                    'value' => $data['action']
                ],
            ];

//Qui funziona

            switch ($data['world']) {
                case 'home-fragrances':
                    $custom_fields[] = [
                        'id' => 5659,
                        'value' => true,
                    ];
                    break;
                case 'make-up-skincare':
                    $custom_fields[] = [
                        'id' => 5657,
                        'value' => true,
                    ];
                    $custom_fields[] = [
                        'id' => 5658,
                        'value' => true,
                    ];
                    break;
                case 'nail-polish':
                    $custom_fields[] = [
                        'id' => 5661,
                        'value' => true,
                    ];
                    break;
                case 'perfumery':
                    $custom_fields[] = [
                        'id' => 5660,
                        'value' => true,
                    ];
                    break;
                case 'personal-care':
                    $custom_fields[] = [
                        'id' => 5662,
                        'value' => true,
                    ];
                    break;
            }

            $args = array(
                'email_address' => $email,
                'subscription' => array(
                    'ip' => $data['ip'],
                    'date' => date("Y-m-d H:i:s"),
                    'status' => "Subscribed"
                ),
                'triggers' => array(
                    'automation' => true,
                    'behaviors' => true
                ),
                'update_if_duplicate' => true,
                'custom_fields' => array_filter($custom_fields, function ($field) {
                    return $field['value'];
                })
            );

            $return = $opidem->subscribeContact($recipient_id, $args);

            if (!is_object($return) && !is_array($return)) {
                return true;
            }
        }

        return false;
    }
}
