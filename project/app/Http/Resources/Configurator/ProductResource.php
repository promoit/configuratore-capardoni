<?php

namespace App\Http\Resources\Configurator;

use App\Components\ConfiguratorState;
use App\Http\Resources\Resource;

class ProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'image' => $this->image,
            'image_url' => $this->image_url,
            'alignment' => $this->alignment,
            'full_image_url' => $this->image_full_url,
            'capardoni_code' => $this->capardoni_code,
            'gallery' => $this->images,
            'web_code' => $this->web_code,
            'name' => $this->name,

            'height' => $this->height_formatted,
            'length' => $this->length_formatted,
            'width' => $this->width_formatted,
            'diameter' => $this->diameter_formatted,
            'suggested_lps' => $this->suggested_lps_formatted,
            'weight' => $this->weight_formatted,
            'dosage' => $this->dosage_formatted,
            'green_option'  => $this->green_option_formatted,

            'attributes' => $this->mapped_attributes,
        ];
    }
}
