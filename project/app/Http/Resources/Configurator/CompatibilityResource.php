<?php

namespace App\Http\Resources\Configurator;

use App\Http\Resources\Resource;

class CompatibilityResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request = null)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'code' => $this->code,
            'image_url' => $this->image_url,
            'gallery' => $this->images,
        ];
    }
}
