<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class DownloadDetails extends Mailable
{
    use Queueable, SerializesModels;
    protected $pdf;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $pdf_name = $this->pdf['pdf_name'];

        if ($this->pdf['combination'] !== false) {
            $pdf_name = preg_replace('/-\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2}/', '', $pdf_name,);
            $pdf_name = Str::replace(['-make-up-skincare', '-nail-polish', '-perfumery', '-home-fragrances', '-personal-care'], '', $pdf_name);
            $pdf_name = Str::replace('-', ' ', $pdf_name);
            $pdf_name = Str::upper($pdf_name);
        } else {
            $pdf_name = 'Capardoni Configuration';
        }

        return $this->from(config('project.email.from.address'), config('project.email.from.name'))
            ->subject("Capardon's Full Pack Configurator - Product details")
            ->view('mails.configuration-download.download')
            ->attach(
                storage_path() . '/app/pdf/' . $this->pdf['pdf_name'] . '.pdf',
                ['as' => "{$pdf_name}.pdf"]
            );
    }
}
