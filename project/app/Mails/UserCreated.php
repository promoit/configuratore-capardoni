<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $user = null;
    public $password = '';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('project.email.from.address'), config('project.email.from.name'))
            ->subject('[Project] Benvenuto!')
            ->markdown('mails.users.created');
    }
}
