<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Role;
use App\Models\Permission;

class SetupRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:roles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup roles and permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Creating admin role...');
        $this->createAdminRole();

        $this->info('Role creation completed!');
    }

    protected function createAdminRole()
    {
        $role = Role::updateOrCreate([
            'name' => Role::ADMIN,
        ], [
            'display_name' => 'Amministratore',
            'description' => 'Amministratore del backend che può gestire utenti e altre entità chiave.',
        ]);
    }
}
