<?php

namespace App\Console\Commands;

use App\Components\ConfiguratorState;
use File;
use Illuminate\Console\Command;

class RenameImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:rename-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rename the uploaded photos based on pattern';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /**
         * FLACONI VETRO:
         * online versione trasparente con seconda immagine versione frosted di tutte le configurazioni
         * - rinomina immagine singola da AMA30F a AMA30N_A
         * - rinomina configurazione da AMA30F_1 a AMA30N_1_A
         *
         * FLACONI PET/PETG:
         * online versione trasparente con seconda immagine versione bianca di tutte le configurazioni
         * - rinomina immagine singola da NY55W a NY55N_A
         * - rinomina configurazione da NY55W_36a NY55N_36_A
         *
         * FLACONI PE:
         * online versione bianca con seconda immagine versione nera di tutte le configurazioni
         * - rinomina immagine singola da PAR100U-24B a PAR100U-24W_A
         * - rinomina configurazione da PAR100U-24B_30 a PAR100U-24W_30_A
         */

        $path = storage_path('app/public/products');
        $images = File::files($path);

        if (empty($images)) {
            $this->warn("Nessuna immagine trovata");
            return 1;
        }

        $products = \App\Models\Product::select('web_code', 'attrib_2')
            ->whereIn('attrib_2', [
                'GLASS',
                'PET',
                'PETG',
                'PE'
            ])
            ->whereIn('type', [
                'LIPSTICK',
                'NAIL POLISH REMOVER',
                ...ConfiguratorState::config('exceptions.pack.filter')
            ])
            ->pluck('attrib_2', 'web_code');

        if ($products->isEmpty()) {
            $this->warn("Nessuna prodotto valido");
            return 1;
        }

        $backupPath = $path . "/renamed";
        $folders = [
            $backupPath,
            "{$path}/CLEAN-CHECK",
            "{$path}/CLEAN",
            "{$path}/FROSTED",
            "{$path}/WHITE",
            "{$path}/BLACK",
        ];

        foreach ($folders as $folder) {
            if (!File::isDirectory($folder)) {
                File::makeDirectory($folder, 0777, true, true);
            } else {
                File::cleanDirectory($folder);
            }
        }

        $this->line("Found " . count($images) . " images");

        $filenames = array_map(fn ($image) => $image->getFilename(), $images);
        $matched = [];
        $skipped = [
            'CLEAN-CHECK' => 0,
            'CLEAN' => 0,
            'FROSTED' => 0,
            'WHITE' => 0,
            'BLACK' => 0,
        ];

        $handle = fopen(storage_path("logs/export-" . now() . ".csv"), 'w');
        fputcsv($handle, ['CODICE PRODOTTO', 'FILE ORIGINALE', 'FILE NUOVO', 'GRUPPO', 'MATERIALE']);

        try {
            foreach ($images as $image) {
                $extension = $image->getExtension();
                if ($extension === 'checksum') {
                    continue;
                }

                $matches = [];
                $filename = $image->getFilename();
                preg_match(
                    "/(?<code>.+)(?<mod>(?<material>[WBFCN])(?<variant>-[0-9]+)?)(?:(?<combination>_[0-9a-z]+)?(?:_(?<increment>[a-z]+))?)?.png$/i",
                    $filename,
                    $matches,
                    PREG_UNMATCHED_AS_NULL
                );

                // Il prodotto non rispetta il pattern base di match
                if (empty($matches)) {
                    $this->warn("Prodotto non corrispondente: " . $filename);
                    $this->copyFile("{$path}/{$filename}", "{$backupPath}/{$filename}");
                    continue;
                }

                unset($matches[0]);
                foreach ($matches as $key => $value) {
                    if (is_numeric($key)) {
                        unset($matches[$key]);
                    }
                }

                $base = $matches['code'] . "%s" . ($matches['variant'] ?: '');

                // Prodotto non rientrato nelle categorie e nei materiali indicati
                if (!$products->contains(function ($value, $key) use ($base) {
                    return preg_match("/" . sprintf($base, "[a-z]{1}") . "/i", $key);
                })) {
                    $this->warn("Prodotto non valido: " . $filename);
                    $this->copyFile("{$path}/{$filename}", "{$backupPath}/{$filename}");
                    continue;
                }

                $newFilename = null;
                $category = null;
                $useIncrement = true;
                $materials = [];

                switch ($matches['material']) {
                    case 'N':
                        $useIncrement = false;
                        if (preg_match("/^(AOS|IBI|VIE|NY100)/i", $matches['code'])) {
                            $category = 'CLEAN-CHECK';
                            $matches['material'] = 'C';
                        }
                        break;
                    case 'C':
                        $useIncrement = false;
                        if (preg_match("/^(AOS|IBI|VIE|NY100)/i", $matches['code'])) {
                            ++$skipped['CLEAN'];
                            $category = 'CLEAN';
                            $matches['material'] = 'N';
                        }
                        break;
                    case 'F':
                        $category = 'FROSTED';
                        $materials = ['GLASS'];
                        $matches['material'] = 'N';
                        break;
                    case 'W':
                        $category = 'WHITE';
                        $materials = ['PET', 'PETG'];
                        $matches['material'] = 'N';
                        break;
                    case 'B':
                        $category = 'BLACK';
                        $materials = ['PE'];
                        $matches['material'] = 'W';
                        break;

                    default:
                        break;
                }

                if (!$category) {
                    $this->warn("Prodotto senza categoria: " . $filename);
                    $this->copyFile("{$path}/{$filename}", "{$backupPath}/{$filename}");
                    continue;
                }

                $product = $base = sprintf($base, $matches['material']);
                $material = $products->first(function ($value, $key) use ($base) {
                    return $key === $base;
                });

                if (!empty($materials)) {
                    if (!$material || !in_array($material, $materials)) {
                        $this->warn($filename . " -> " . $base . " -> " . $material);
                        $this->copyFile("{$path}/{$filename}", "{$backupPath}/{$filename}");
                        continue;
                    }
                }

                $newFilename = $base .= $matches['combination'];
                if ($matches['increment']) {
                    $newFilename .= "_{$matches['increment']}";
                }
                $newFilename .=  ".png";

                if ($category === 'CLEAN-CHECK') {
                    if (!in_array($newFilename, $filenames)) {
                        $this->warn("{$filename} -> $newFilename");
                        $this->copyFile("{$path}/{$filename}", "{$backupPath}/{$filename}");
                    } else {
                        ++$skipped['CLEAN-CHECK'];
                        $this->copyFile("{$path}/{$filename}", "{$path}/{$category}/{$newFilename}");
                    }

                    continue;
                }

                if ($useIncrement) {
                    $letter = $matches['increment'] ?: "A";

                    while (in_array($newFilename, $filenames)) {
                        $newFilename = $base . "_" . ($letter++) . ".png";
                    }
                }

                $filenames[] = $newFilename;
                $matched[] = $product;

                $this->info("{$category}: {$filename} -> {$newFilename}");

                fputcsv($handle, [$product, $filename, $newFilename, $category, $material]);

                $this->copyFile("{$path}/{$filename}", "{$path}/{$category}/{$newFilename}");
                $this->copyFile("{$path}/{$filename}", "{$backupPath}/{$newFilename}");
                ++$skipped[$category];
            }
        } catch (\Throwable $th) {
            $this->error($th->getMessage());
            die;
        }

        fclose($handle);

        $unique = array_values(array_unique($matched));
        dump($skipped);
        // dump($unique);
        $this->info(count($unique) . " : " . count($matched));

        return 0;
    }

    private function copyFile($from, $to)
    {
        if (File::exists($to)) {
            throw new \Exception("File già presente: {$from} -> {$to}", 1);
        }

        $bool = File::copy($from, $to);
        if (!$bool) {
            throw new \Exception("Errore durante spostamento: {$from} -> {$to}", 1);
        }

        return true;
    }
}
