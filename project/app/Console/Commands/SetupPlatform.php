<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Role;

class SetupPlatform extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:platform {--rebuild} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup platform data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (App::environment('production') && !$this->option('force')) {
            $this->error("You can't do this in production");
            return;
        }

        $this->call('migrate:fresh');

        $this->call('setup:roles');

        $user = User::create([
            'name' => 'Filippo Toso',
            'username' => 'filippo.toso',
            'email' => 'filippo@toso.dev',
            'password' => Hash::make('8fr3zWmHmZzyJbKR'),
            'email_verified_at' => Carbon::now(),
        ]);

        $user->attachRoles(Role::pluck('id'));

        if ($this->option('rebuild')) {
            $this->call('generate:models', ['--overwrite' => 'all']);
        }
    }
}
