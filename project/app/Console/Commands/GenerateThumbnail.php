<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use File;
use Image;
use Storage;
use Imagick;
use App\Models\ImgAlignment;
use App\Models\Support\Product;

class GenerateThumbnail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:generate-thumbnail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a thumbnail version of uploaded photos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = storage_path('app/public/products');
        $files = File::files($path);

        $images = array_filter($files, function ($file) {
            $extension = $file->getExtension();
            return $extension !== 'checksum' && $extension !== 'jpg';
        });

        if (!empty($images)) {
            $thumbnailPath = $path . "/thumbnail";
            $previewPath = $path . "/preview";

            if (!File::isDirectory($thumbnailPath)) {
                File::makeDirectory($thumbnailPath, 0777, true, true);
            }

            if (!File::isDirectory($previewPath)) {
                File::makeDirectory($previewPath, 0777, true, true);
            }

            $this->line("Found " . count($images) . " images");

            $alredyDone = array();

            foreach ($images as $index => $image) {
                $extension = $image->getExtension();

                $hash = md5_file($image->getPathname());

                $checksumFile = str_ireplace(".".$extension, '.checksum', $image->getFilename());
                $checksumPath = "{$path}/{$checksumFile}";
                $newPath = "{$thumbnailPath}/{$image->getFilename()}";
                $newPathPreview = "{$previewPath}/{$image->getFilename()}";


                $currentAlign = ImgAlignment::where('img', '=', $image->getFilename());
                $currentAlignHash = ImgAlignment::where('img', '=', $image->getFilename())->where('checksum', '=', $hash);


                if (!$currentAlign->exists() || !$currentAlignHash->exists() && !isset($alredyDone[$image->getFilename()])) {

                    $alredyDone[$image->getFilename()] = 1;

                    $img = new Imagick($image->getPathName());

                    $imag = array();
                    $imag['h'] = $img->getImageHeight();
                    $imag['w'] = $img->getImageWidth();

                    $pixel_iterator = $img->getPixelIterator();
                    foreach ($pixel_iterator as $y => $pixels) {
                        foreach ($pixels as $x => $pixel) {
                            $color = $pixel->getColor(true);

                            if ($color['a'] != 0 && !isset($imag['yt']))
                                $imag['yt'] = ($y);
                            else
                                    if ($color['a'] != 0)
                                $imag['yb'] = ($y);
                        }
                    }


                    if ($currentAlign->exists()) {
                        $currentAlign->delete();
                        $this->info("Update Img Alignment: {$image->getFilename()}");
                    } else {
                        $this->info("Generate Img Alignment: {$image->getFilename()}");
                    }

                    $recordId = ImgAlignment::create([
                        'img' => $image->getFilename(),
                        'checksum' => $hash,
                        'w' => $imag['w'],
                        'h' => $imag['h'],
                        'yt' => 100 * $imag['yt'] / $imag['h'],
                        'yb' => 100 * ($imag['h'] - $imag['yb']) / $imag['h'],
                    ])->id;
                } else {
                    // ESISTE E HA LO STESSO HASH QUINDI NON FACCIO NULLA
                    $this->comment("Img Alignment skipped: {$image->getFilename()}");
                }

                $selectedProduct = Product::where('image', pathinfo($image, PATHINFO_FILENAME))->whereNull('img_alignment_id')->first();
                if($selectedProduct){
                    if(!isset($recordId)){
                        $recordId = ImgAlignment::where('img', $image->getFilename())->first()->id;
                    }
                    $selectedProduct->img_alignment_id = $recordId;
                    $returnValue = $selectedProduct->save();
                    if($returnValue){
                        $this->info("Associato id allineamento immagine: {$image->getFilename()}");
                    }else{
                        $this->comment("Errore durante associazione allineamento: {$image->getFilename()}");
                    }
                }
                $recordId = null;

                $generateThumb = $this->generateImage($image, $newPath, 500, $checksumPath, $hash, 'Thumbnail');
                $generatePreview = $this->generateImage($image, $newPathPreview, 260, $checksumPath, $hash, 'Preview');

                if($generateThumb && $generatePreview){
                    Storage::disk('public')->put("products/{$checksumFile}", $hash);
                }

            }

            return 0;
        }

        return 1;
    }

    public function generateImage($image, $newPath, $size, $checksumPath, $hash, $description) {
        if (File::exists($newPath) && File::exists($checksumPath) && File::get($checksumPath) === $hash) {
            $this->comment("{$description} skipped: {$image->getFilename()}");
            return false;
        } else {
            try {
                Image::make($image)
                    ->resize($size, $size, function ($const) {
                        $const->aspectRatio();
                    })
                    ->save($newPath);

                $this->info("Generate new {$description}: {$image->getFilename()}");
            } catch (Exception $error) {
                $this->error("Error generating {$description}: {$image->getFilename()}: {$error->getMessage()}");
                return false;
            }
        }
        return true;
    }
}
