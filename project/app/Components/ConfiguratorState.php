<?php

namespace App\Components;

use App\Http\Resources\Configurator\CompatibilityResource;
use App\Models\Compatibility;
use App\Models\Product;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;

class ConfiguratorState
{
    protected const DEFAULT_FILE = 'app/default.php';
    protected const CONFIG_FILE = 'app/configurator.php';

    public static function make()
    {
        return new static();
    }

    public function state($world, $filters = [])
    {
        $filters = $this->filters($world, $filters);

        $selectedProducts = $this->selectedProducts($world, $filters);

        return [
            'categories' => $this->categories($world, $filters),
            'filters' => $filters,
            'products' => $selectedProducts,
            'combination' => $this->combination($world, $selectedProducts),
        ];
    }

    public function firstState($world, $filters = [])
    {
        $filters = $this->filters($world, $filters);

        $selectedProducts = $this->selectedProducts($world, $filters);

        return [
            'categories' => [],//$this->categories($world, $filters),
            'filters' => $filters,
            'products' => $selectedProducts,
            'combination' => null,//$this->combination($world, $selectedProducts),
        ];
    }

    public function filtersFromProducts($products, $loadedFilters = [])
    {
        $config = ConfiguratorState::config('filters');
        $options = ConfiguratorState::config('options');

        $filters = [];

        foreach ($products as $category => $product) {
            $filters[$category] = [
                'product' => $product,
                'filters' =>  array_fill_keys(array_keys($config[$category] ?? []), null),
            ];
        }

        foreach ($loadedFilters as $category => $attributes) {
            foreach ($attributes as $attribute => &$value) {
                if (!array_key_exists($category, $config)) {
                    continue;
                }

                if (!array_key_exists($attribute, $config[$category])) {
                    continue;
                }

                if (!array_key_exists($config[$category][$attribute], $options)) {
                    continue;
                }

                if (array_key_exists('checkbox', $options[$config[$category][$attribute]])) {
                    $value = boolval($value);
                }
            }

            if (array_key_exists($category, $filters)) {
                $filters[$category]['filters'] = array_merge($filters[$category]['filters'], $attributes);
            } else {
                $filters[$category] = [
                    'product' => null,
                    'filters' => array_merge(
                        array_fill_keys(array_keys($config[$category] ?? []), null),
                        $attributes
                    ),
                ];
            }
        }

        return $filters;
    }

    protected function filters($world, $filters)
    {
        $categoriesCfg = static::config('categories.' . $world);
        $filtersCfg = static::config('filters');

        $results = [];

        foreach ($categoriesCfg as $category) {

            // Logic to handle pack exception
            if ($category['pack'] ?? false) {
                $filterCfg = static::config('exceptions.pack.filters');
            } else {
                $type = array_first($category['types']);
                $filterCfg = $filtersCfg[$type] ?? [];
            }

            $defaultFilters = array_fill_keys(array_keys($filterCfg), null);

            $results[$category['name']] = [
                'name' => $category['name'],
                'product' => $filters[$category['name']]['product'] ?? null,
                'filters' => array_merge($defaultFilters, $filters[$category['name']]['filters'] ?? []),
            ];
        }

        return $results;
    }

    protected function emptyProducts($filters)
    {
        foreach ($filters as $current) {
            if ($current['product'] ?? null) {
                return false;
            }
        }

        return true;
    }

    protected function products($world, $filters, $forFilters = false)
    {
        if ($this->emptyProducts($filters)) {
            return $this->mandatoryProducts($world, $filters);
        }

        return $this->filteredProducts($world, $filters, $forFilters);
    }

    protected function mandatoryProducts($world,  $filters)
    {
        $cacheKey = "products_of_{$world}";

        $categoriesCfg = collect(static::config('categories.' . $world))->mapWithKeys(function ($categoryCfg) {
            return [$categoryCfg['name'] => $categoryCfg['types']];
        });

        // This will contain all the compatibilities and the filters required
        $compatibilities = $this->compatibilitiesFromFilters($filters);

        $filters = array_column($compatibilities, 'filters');
        $showAll = empty(array_filter($filters));

        if ($showAll && Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $query = Product::select('id', 'type', 'web_code')
            ->whereHas('compatibilities', function ($query) use ($world) {
                $query->whereWorld($world);
            })
            ->orderBy('name');

        $query->where(function ($query) use ($compatibilities, $categoriesCfg) {

            foreach ($compatibilities as $type => $compatibility) {

                $query->orWhere(function ($query) use ($type, $compatibility, $categoriesCfg) {
                    // Filter by current filter types
                    $query->whereIn('type', $categoriesCfg[$type]);

                    $attributesToExcludes = static::config('excludes.' . $type);
                    $query = $this->buildQueryWithExclusion($query, $attributesToExcludes, $compatibility);
                    // dd(
                    //     'mandatoryProducts',
                    //     $attributesToExcludes,
                    //     str_replace_array('?', $query->getBindings(), $query->toSql())
                    // );

                    // Filter by current attributes
                    foreach ($compatibility['filters'] as $field => $value) {
                        if (is_array($value)) {
                            if (!empty($value)) {
                                $query->whereIn($field, $value);
                            }

                            continue;
                        }

                        if (is_bool($value)) {
                            $field_i = strtoupper(str_replace('_', ' ', $field));
                            $checkbox_value = (static::config("options.{$field_i}.checkbox"));

                            if ($value === true && !empty($checkbox_value)) {
                                $query->whereIn($field, $checkbox_value);
                            }


                            continue;
                        }

                        $query->where($field, '=', $value);
                    }
                });
            }
        });

        $products = $query->get()
            ->mapToGroups(function ($record) {
                return [$record->type => $record->web_code];
            })
            ->toArray();

        if ($showAll) {
            Cache::forever($cacheKey, $products);
        }

        return $products;
    }

    /*
    protected function mandatoryTypes($world)
    {
        $types = Product::whereHas('compatibilities', function ($query) use ($world) {
            $query->whereWorld($world);
        })
            ->distinct()
            ->pluck('type');

        $mandatory = collect(static::config('mandatory'));

        return $types->intersect($mandatory);
    }
    // */

    protected function filteredProducts($world, $filters, $forFilters = false)
    {
        $categoriesCfg = collect(static::config('categories.' . $world))->mapWithKeys(function ($categoryCfg) {
            return [$categoryCfg['name'] => $categoryCfg['types']];
        });

        // This will contain all the compatibilities and the filters required
        $compatibilities = $this->compatibilitiesFromFilters($filters);

        $query = Product::query();

        foreach ($compatibilities as $type => $compatibility) {
            $query->orWhere(function ($query) use ($world, $type, $compatibility, $categoriesCfg, $forFilters) {

                // Filter by current filter types
                $query->whereIn('type', $categoriesCfg[$type]);

                // Filter by other types compatibilities
                $query->whereHas('compatibilities', function ($query) use ($world, $compatibility) {
                    $query->whereWorld($world);

                    foreach ($compatibility['products'] as $product) {
                        $query->whereHas('products', function ($query) use ($product) {
                            $query->whereWebCode($product);
                        });
                    }
                });

                if (!$forFilters) {
                    // Filter by current attributes
                    foreach ($compatibility['filters'] as $field => $value) {
                        if (is_array($value)) {
                            if (!empty($value)) {
                                $query->whereIn($field, $value);
                            }

                            continue;
                        }

                        if (is_bool($value)) {
                            $field_i = strtoupper(str_replace('_', ' ', $field));
                            $checkbox_value = (static::config("options.{$field_i}.checkbox"));

                            if ($value === true && !empty($checkbox_value)) {
                                $query->whereIn($field, $checkbox_value);
                            }


                            continue;
                        }

                        $query->where($field, '=', $value);
                    }
                }

                $attributesToExcludes = static::config('excludes.' . $type);
                $query = $this->buildQueryWithExclusion($query, $attributesToExcludes, $compatibility);
                // dd(
                //     'filteredProducts',
                //     $attributesToExcludes,
                //     str_replace_array('?', $query->getBindings(), $query->toSql())
                // );
            });

            // Always show the current selected
            if ($compatibility['product']) {
                $query->orWhere('web_code', '=', $compatibility['product']);
            }
        }

        $products = $query->orderBy('name')
            ->select('id', 'type', 'web_code')
            ->get();

        $results = $products->mapToGroups(function ($product) {
            return [$product->type => $product->web_code];
        })->toArray();

        return $results;
    }

    protected function compatibilitiesFromFilters($filters)
    {
        $compatibilityByType = [];

        foreach ($filters as $type => $filter) {
            if ($filter['product']) {
                $compatibilityByType[$type] = $filter['product'];
            }
        }

        $compatibilities = [];

        foreach ($filters as $type => $filter) {

            $productsByType = Arr::except($compatibilityByType, [$type]);

            $filtersByType = array_filter($filter['filters'], function ($value) {
                return !is_null($value) && ($value !== '');
            });

            $compatibilities[$type] = [
                'product' => $filter['product'],
                'products' => $productsByType,
                'filters' => $filtersByType,
            ];
        }

        return $compatibilities;
    }

    protected function selectedProducts($world, $filters)
    {
        $productCodes = collect($filters)->map(function ($filter) {
            return $filter['product'];
        })->filter();

        return Product::whereHas('compatibilities', function ($query) use ($world) {
            $query->whereWorld($world);
        })->whereIn('web_code', $productCodes)
            ->pluck('web_code')
            ->toArray();
    }

    protected function combination($world, $products)
    {
        if (!empty($products)) {

            $count = count($products);

            $query = Compatibility::whereWorld($world)
                ->withCount('products')
                ->has('products', '=', $count);

            foreach ($products as $product) {
                $query->whereHas('products', function ($query) use ($product) {
                    $query->where('web_code', '=', $product);
                });
            }

            if ($compatibility = $query->first()) {
                return (new CompatibilityResource($compatibility))->toArray();
            }
        }

        return null;
    }

    protected function categories($world, $filters)
    {
        if ($this->emptyProducts($filters)) {
            $categories = $this->mandatoryCategories($world);
        } else {
            $categories = $this->filteredCategories($world);
        }

        $products = $this->products($world, $filters);
        $productsForFilters = $this->products($world, $filters, true);

        $results = [];

        foreach ($categories as $category) {
            $current = [
                'id' => sha1($category['name']),
                'name' => $category['name'],
                'filters' => [],
                'products' => [],
                'product' => $filters[$category['name']]['product'] ?? null,
                'aligned_to' => isset($category['aligned_to']) && in_array($category['aligned_to'], ['top', 'bottom', 'middle']) ? $category['aligned_to'] : 'middle',
            ];

            $productsForFilter = [];

            foreach ($category['types'] as $type) {
                $current['products'] = array_merge($current['products'], $products[$type] ?? []);
                $productsForFilter = array_merge($productsForFilter, $productsForFilters[$type] ?? []);
            }

            $current['filters'] = $this->filtersForCategory($category, $filters, $productsForFilter, $world);

            $results[] = $current;
        }

        return $results;
    }

    protected function filtersForCategory($category, $filter, $products, $world)
    {
        $results = [];
        $optionsCfg = static::config('options');
        $filtersToUse = [
            'single' => [],
            'multiple' => [],
        ];
        // $filtersToUse = $filter['filters'];

        if ($category['pack'] ?? false) {
            $results['pack'] = [
                'name' => 'type',
                'label' => 'TYPE',
                'options' => $this->options(static::config('exceptions.pack.filter')),
            ];

            $filtersCfg = static::config('exceptions.pack.filters', []);
        } else {
            $filtersCfg = static::config('filters.' . $category['name'], []);
        }

        if (empty($filtersCfg)) {
            return $results;
        }

        foreach ($filtersCfg as $field => $label) {
            $results[$field] = [
                'name' => $field,
                'label' => $label,
                'options' => [],
            ];

            if (array_key_exists($label, $optionsCfg)) {
                if (array_key_exists('multiple', $optionsCfg[$label])) {
                    // if (array_key_exists('multiple', $optionsCfg[$label]) && !empty(array_filter($filtersToUse))) {
                    $filtersToUse['multiple'][$field] = $label;
                }

                $results[$field] = array_merge($optionsCfg[$label], $results[$field]);
            }

            if (!in_array($field, $filtersToUse['multiple'])) {
                $filtersToUse['single'][$field] = $label;
            }
        }

        foreach ($filtersToUse as $option => $filters) {
            if (empty($filters)) {
                continue;
            }

            $queries = [];

            $productsToUse = $products;
            if ($option === 'multiple') {
                $filter[$category['name']]['filters'] = array_merge(
                    $filter[$category['name']]['filters'],
                    array_map(fn ($filter) => null, $filters)
                );
                $productsForFilters = $this->products($world, $filter, true);
                $productsToUse = [];

                foreach ($category['types'] as $type) {
                    $productsToUse = array_merge($productsToUse, $productsForFilters[$type] ?? []);
                }
            }

            $query = Product::whereIn('web_code', $productsToUse)
                ->orderBy('value')
                ->distinct();

            $fields = array_intersect(array_keys($filters), array_keys($filtersCfg));

            foreach ($fields as $field) {
                $current = clone $query;
                $current->selectRaw('"' . $field . '" as field, ' . $field . ' as value')
                    ->whereNotNull($field);

                $queries[] = $current;
            }

            if ($query = array_shift($queries)) {
                foreach ($queries as $current) {
                    $query->union($current);
                }
            }

            $records = $query->get();

            foreach ($records as $record) {
                $value = $record->value;

                if (is_numeric($value)) {
                    $value = floatval($value) * 100;
                }

                $results[$record->field]['options'][$value] = [
                    'label' => $record->value,
                    'value' => $record->value,
                ];
            }
        }

        foreach ($results as $index => &$result) {
            $options = $result['options'];

            if ($index == "attrib_3") {
                $option_tmp = array();

                foreach ($options as $k2 => $v2) {
                    $chiave = preg_replace('/ml/', '', $k2);
                    $chiave = preg_replace('/,/', '.', $chiave);
                    $option_tmp[floatval($chiave) * 100] = $v2;
                }

                ksort($option_tmp);
                $option = array();

                $options = $option_tmp;
            } else {
                ksort($options);
            }

            if ($index == 'suggested_lps') {
                foreach ($options as $k3 => $v3) {
                    $label =  $v3['label'];

                    if (!$label) {
                        continue;
                    }

                    if (floatval($label) === floatval(intval($label))) {
                        $label = intval($label);
                    }

                    $options[$k3]['label'] = "{$label}mm";
                }
            }

            $result['options'] = array_values($options);
        }

        return array_values($results);
    }

    protected function options($options)
    {
        $results = [];

        foreach ($options as $label) {
            $results[] = [
                'label' => $label,
                'value' => $label,
            ];
        }

        return $results;
    }

    protected function mandatoryCategories($world)
    {
        $categories = static::config('categories.' . $world);
        $mandatory = static::config('mandatory');

        return collect($categories)->filter(function ($category) use ($mandatory) {
            $intersect = array_intersect($category['types'], $mandatory);
            return !empty($intersect);
        })->toArray();
    }

    protected function filteredCategories($world)
    {
        return static::config('categories.' . $world);
    }

    public static function config($key = null, $default = null)
    {
        $path = storage_path(static::CONFIG_FILE);
        $config =  file_exists($path) ? include($path) : [];
        return $key ? Arr::get($config, $key, $default) : $config;
    }

    public static function initialize()
    {
        $default = storage_path(static::DEFAULT_FILE);
        $configurator = storage_path(static::CONFIG_FILE);

        if (!file_exists($configurator)) {
            copy($default, $configurator);
        }
    }

    private function buildQueryWithExclusion($query, $attributesToExcludes, $compatibility)
    {
        if (empty($attributesToExcludes)) {
            return $query;
        }

        foreach ($attributesToExcludes as $field => $value) {
            if (array_key_exists($field, $compatibility['filters'])) {
                continue;
            }

            $query->where(function ($query) use ($field, $value) {
                // Questa parte serve per escludere i prodotti che hanno più condizioni (es: materiale/colore)
                if (Arr::isAssoc($value)) {
                    $query->whereNotIn('id', function ($query) use ($value) {
                        $query->select('id')->from('products');

                        foreach ($value as $subField => $subValue) {
                            if (is_array($subValue)) {
                                $query->whereIn($subField, $subValue);
                            } else {
                                $query->where($subField, $subValue);
                            }
                        }
                    });

                    return;
                }

                if (is_array($value)) {
                    $query->where(function ($query) use ($field, $value) {
                        foreach ($value as $single) {
                            $query->where($field, 'NOT LIKE', $single);
                        }
                    });
                    // $query->whereNotIn($field, $value);
                } else {
                    $query->where($field, 'NOT LIKE', $value);
                }

                $query->orWhereNull($field);
            });
        }

        return $query;
    }
}
