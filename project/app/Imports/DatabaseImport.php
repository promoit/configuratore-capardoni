<?php

namespace App\Imports;

use App\Imports\Sheets;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class DatabaseImport implements WithMultipleSheets
{
    use Importable;

    protected $sheets;

    public function __construct()
    {
        DB::table('product_compatibility')->delete();
        DB::table('products')->delete();
        DB::table('compatibilities')->delete();
    }

    public function sheets(): array
    {
        $this->sheets = [
            'ARTICOLI SINGOLI' => new Sheets\ProductsImport(),
            'COMPATIBILITY' => new Sheets\CompatibilitiesImport(),
        ];

        return $this->sheets;
    }

    public function failures()
    {
        $failures = [];

        foreach ($this->sheets as $name => $sheet) {
            $failures[$name] = $sheet->failures();
        }

        return $failures;
    }
}
