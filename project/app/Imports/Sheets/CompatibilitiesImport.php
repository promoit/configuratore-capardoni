<?php

namespace App\Imports\Sheets;

use App\Models\Compatibility;
use App\Models\Product;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Validators\Failure;

class CompatibilitiesImport implements ToCollection, WithStartRow, WithHeadingRow, SkipsEmptyRows, SkipsOnFailure
{
    use Importable, SkipsFailures;

    protected $mappings = [];

    public function collection(Collection $rows)
    {
        // Must be here to be sure the products has been already imported
        $this->mappings = Product::pluck('id', 'web_code')->toArray();

        foreach ($rows as $row) {

            $compatibility = Compatibility::create([
                'skincare' => (bool)$row['skincare'],
                'personal_care' => (bool)$row['pc'],
                'nail_polish' => (bool)$row['nail_polish'],
                'make_up' => false,
                'perfumery' => (bool)$row['perfumery'],
                'home_fragrances' => (bool)$row['hf'],
                'caps_for_alu_tube' => (bool)$row['caps_for_alu_tube'],
                'title' => $row['combination_title'],
                'code' => $row['combination_code'],
                'image' => $row['image'],
            ]);

            $productKeys = [
                'product_code_1', 'product_code_2', 'product_code_3', 'product_code_4',
                'product_code_5', 'product_code_6', 'product_code_7', 'product_code_8',
                'product_code_9', 'product_code_10', 'product_code_11', 'product_code_12',
                'product_code_13', 'product_code_14', 'product_code_15', 'product_code_16',
                'product_code_17', 'product_code_18', 'product_code_19', 'product_code_20',
                'product_code_21', 'product_code_22', 'product_code_23', 'product_code_24',
                'product_code_25', 'product_code_26', 'product_code_27',
            ];

            foreach ($productKeys as $key) {
                // If not all products a present, delete the cobination
                if (!$this->attach($compatibility, $key, $row[$key])) {
                    $compatibility->products()->detach();
                    $compatibility->delete();
                    break;
                }
            }
        }
    }

    protected function attach(Compatibility $compatibility, $key, $value)
    {
        if (is_null($value)) {
            return true;
        }

        $mappedId = $this->mappings[$value] ?? null;

        if (is_null($mappedId)) {
            $failure = new Failure(
                0,
                $key,
                [$value]
            );

            $this->onFailure($failure);

            return false;
        } else {
            $type = $key; // Implement this if requird!
            $compatibility->products()->attach($mappedId, ['type' => $type]);
        }

        return true;
    }

    public function headingRow(): int
    {
        return 3;
    }

    public function startRow(): int
    {
        return 4;
    }
}
