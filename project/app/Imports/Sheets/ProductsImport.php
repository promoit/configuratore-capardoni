<?php

namespace App\Imports\Sheets;

use App\Models\Product;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductsImport implements ToCollection, WithHeadingRow, SkipsOnFailure
{
    use Importable, SkipsFailures;

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {

            if (is_null($row['image_name_webcode'])) {
                continue;
            }


            $ret = Product::updateOrCreate([
                'web_code' => $row['image_name_webcode'],
                'type' => $row['type'],
            ], [
                'image' => $row['image_name_webcode'],
                'capardoni_code' => $row['capardoni_code'],
                'name' => $row['name'],
                'height' => $this->normalize($row['altezza_mm']),
                'diameter' => $this->normalize($row['diametro_mm']),
                'length' => $this->normalize($row['lunghezza']),
                'width' => $this->normalize($row['larghezza']),
                'suggested_lps' => $this->normalize($row['lps_consigliata_mm']),
                'weight' => $this->normalize($row['peso_gr']),
                'dosage' => $this->normalize($row['dosaggio_cc']),
                'green_option' => $this->normalize($row['green_option']),
                'attrib_1' => $this->normalize($row['attrib_1']),
                'attrib_2' => $this->normalize($row['attrib_2']),
                'attrib_3' => $this->normalize($row['attrib_3']),
                'attrib_4' => $this->normalize($row['attrib_4']),
                'attrib_5' => isset($row['attrib_5']) ? $this->normalize($row['attrib_5']) : null, // Temporary not present
                'attrib_6' => $this->normalize($row['attrib_6']),
                'attrib_7' => $this->normalize($row['attrib_7']),
                'attrib_8' => $this->normalize($row['attrib_8']),
            ]);
        }
    }

    public function headingRow(): int
    {
        return 2;
    }

    protected function normalize($string)
    {
        $errors = ['TBC', 'CHECK DT', 'NOT AVAILABLE', 'NOT AVAILABE', 'N/A', '-'];

        if (is_string($string)) {
            return in_array(trim($string), $errors) ? null : trim($string);
        }

        return in_array($string, $errors) ? null : $string;
    }
}
