<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $templates = [
            'value' => 'template.form.value',
            'text' => 'template.form.text',
            'file' => 'template.form.file',
            'email' => 'template.form.email',
            'hidden' => 'template.form.hidden',
            'password' => 'template.form.password',
            'checkbox' => 'template.form.checkbox',
            'textarea' => 'template.form.textarea',
            'select' => 'template.form.select',
            'submit' => 'template.form.submit',
            'wysiwyg' => 'template.form.wysiwyg',

            // 'radio' => 'template.form.radio',
            // 'checkboxes' => 'template.form.checkboxes',
            // 'button' => 'template.form.button',
        ];

        foreach ($templates as $directive => $path) {
            Blade::directive($directive, function ($expression) use ($path) {
                return "<?php echo \$__env->make('{$path}', ['field' => field({$expression})], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>";
            });
        }
    }

}
