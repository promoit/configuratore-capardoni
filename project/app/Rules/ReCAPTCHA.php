<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Http;

class ReCAPTCHA implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $response = Http::acceptJson()
            ->asForm()
            ->withoutVerifying()
            ->post('https://www.google.com/recaptcha/api/siteverify', [
                'secret' => config('project.google.recaptha.v3.private'),
                'response' => $value,
                'remoteip' => request()->ip(),
            ]);

        return $response->object()->success;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.recaptcha');
    }
}
