<?php

use App\Models\Compatibility;

return [

    'exceptions' => [
        'pack' => [
            // Additional pack filter when the 'pack' field is true in the categories array
            'filter' => [
                'AIRLESS BOTTLE', 'AIRLESS JAR', 'GLASS BOTTLE', 'PLASTIC BOTTLE', 'GLASS JAR', 'PLASTIC JAR', 'LIP GLOSS / CONCEALER', 'LIPSTICK', 'ROLL-ON'
            ],
            'filters' => [
                'attrib_3' => 'SIZE',
                'attrib_2' => 'MATERIALS',
                'attrib_1' => 'NECK',
                'attrib_6' => 'SHAPE',
                'attrib_4' => 'FINISHING',
                'suggested_lps' => 'LPS',
                // 'attrib_5' => null,
            ],
        ],
    ],

    'worlds' => [
        Compatibility::WORLD_SKINCARE => 'Skincare & Make-up',
        Compatibility::WORLD_NAIL_POLISH => 'Nail Polish',
        Compatibility::WORLD_PERFUMERY => 'Perfumery',
        Compatibility::WORLD_HOME_FRAGRANCES => 'Home Fragrances',
        Compatibility::WORLD_PERSONAL_CARE => 'Personal Care',
        Compatibility::WORLD_CAPS_FOR_ALU_TUBES => 'Caps for alu tubes',
    ],

    'worlds_fields' => [
        Compatibility::WORLD_SKINCARE => ["minimum_qty" => "10k"],
        Compatibility::WORLD_NAIL_POLISH => ["minimum_qty" => "10k"],
        Compatibility::WORLD_PERFUMERY => ["minimum_qty" => "10k"],
        Compatibility::WORLD_HOME_FRAGRANCES => ["minimum_qty" => "10k"],
        Compatibility::WORLD_PERSONAL_CARE => ["minimum_qty" => "10k"],
        Compatibility::WORLD_CAPS_FOR_ALU_TUBES => ["minimum_qty" => "10k"],
    ],

    'disabled' => [
        Compatibility::WORLD_SKINCARE => false,
        Compatibility::WORLD_NAIL_POLISH => false,
        Compatibility::WORLD_PERFUMERY => false,
        Compatibility::WORLD_HOME_FRAGRANCES => false,
        Compatibility::WORLD_PERSONAL_CARE => false,
        Compatibility::WORLD_CAPS_FOR_ALU_TUBES => true,
    ],

    // Mandatory categories on startup (when no product or filter is selected)
    'mandatory' => [
        'BOTTLE', 'JAR', 'PLASTIC BOTTLE', 'PLASTIC JAR', 'GLASS BOTTLE', 'GLASS JAR', 'AIRLESS BOTTLE', 'AIRLESS JAR', 'AIRLESS JAR', 'LIP GLOSS / CONCEALER', 'GLOSS ACCESSORIES', 'DROPPER', 'LIPSTICK', 'SPRAY PUMP', 'LOTION PUMP', 'SPRAY PUMP + OC',
        'LOTION PUMP + OC', 'CAP', 'CAPS', 'BRUSHES', 'COLLARS', 'STICKS', 'FLIP-TOP', 'DISC-TOP', 'DISPENSER PUMP', 'TRIGGER', 'FOAMER PUMP', 'POWDER PUMP', 'NAIL POLISH CAPS', 'ROLL-ON', 'NAIL POLISH REMOVER', 'SAMPLING'
    ],

    // The order here decides in wich order the categories will displayed in the configurator
    'categories' => [
        Compatibility::WORLD_SKINCARE => [
            [
                'name' => 'PACK',
                'types' => ['AIRLESS BOTTLE', 'AIRLESS JAR', 'PLASTIC BOTTLE', 'PLASTIC JAR', 'GLASS BOTTLE', 'GLASS JAR', 'LIP GLOSS / CONCEALER', 'LIPSTICK', 'ROLL-ON'],
                'pack' => true,
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'BRUSHES',
                'types' => ['BRUSHES'],
            ],
            [
                'name' => 'CAP',
                'types' => ['CAP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'CAPS',
                'types' => ['CAPS'],
            ],
            [
                'name' => 'COLLARS',
                'types' => ['COLLARS'],
            ],
            [
                'name' => 'DISC-TOP',
                'types' => ['DISC-TOP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'DISPENSER PUMP',
                'types' => ['DISPENSER PUMP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'DROPPER',
                'types' => ['DROPPER'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'FLIP-TOP',
                'types' => ['FLIP-TOP'],
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'GLOSS ACCESSORIES',
                'types' => ['GLOSS ACCESSORIES'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'LOTION PUMP',
                'types' => ['LOTION PUMP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'LOTION PUMP + OC',
                'types' => ['LOTION PUMP + OC'],
            ],
            [
                'name' => 'LOTION SPRAY PUMP + OC',
                'types' => ['LOTION SPRAY PUMP + OC'],
            ],
            [
                'name' => 'OVERCAP SPRAY',
                'types' => ['OVERCAP SPRAY'],
            ],

            [
                'name' => 'OVERCAP LP',
                'types' => ['OVERCAP LP'],
            ],
            [
                'name' => 'OVERCAP LOTION',
                'types' => ['OVERCAP LOTION'],
            ],

            [
                'name' => 'OVERCAPS NAIL POLISH',
                'types' => ['OVERCAPS NAIL POLISH'],
            ],
            [
                'name' => 'NAIL POLISH CAPS',
                'types' => ['NAIL POLISH CAPS'],
            ],
            [
                'name' => 'NAIL POLISH OVERCAPS',
                'types' => ['NAIL POLISH OVERCAPS'],
            ],
            [
                'name' => 'NAIL POLISH BRUSH',
                'types' => ['NAIL POLISH BRUSH'],
            ],
            [
                'name' => 'FOAMER PUMP',
                'types' => ['FOAMER PUMP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'POWDER PUMP',
                'types' => ['POWDER PUMP'],
            ],
            [
                'name' => 'PUMP OVERCAP',
                'types' => ['PUMP OVERCAP'],
            ],
            [
                'name' => 'SPRAY PUMP + OC',
                'types' => ['SPRAY PUMP + OC'],
            ],
            [
                'name' => 'SPRAY PUMP',
                'types' => ['SPRAY PUMP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'SHIVE',
                'types' => ['SHIVE'],
            ],
            [
                'name' => 'STICKS',
                'types' => ['STICKS'],
            ],
            [
                'name' => 'SPONGE',
                'types' => ['SPONGE'],
            ],
            [
                'name' => 'PLUG',
                'types' => ['PLUG'],
            ],
            [
                'name' => 'TRIGGER',
                'types' => ['TRIGGER'],
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'WIPER',
                'types' => ['WIPER'],
            ],
        ],
        Compatibility::WORLD_PERSONAL_CARE => [
            [
                'name' => 'BOTTLE',
                'types' => ['BOTTLE'],
            ],
            [
                'name' => 'GLASS BOTTLE',
                'types' => ['GLASS BOTTLE'],
            ],
            [
                'name' => 'PLASTIC BOTTLE',
                'types' => ['PLASTIC BOTTLE'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'BRUSHES',
                'types' => ['BRUSHES'],
            ],
            [
                'name' => 'CAP',
                'types' => ['CAP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'CAPS',
                'types' => ['CAPS'],
            ],
            [
                'name' => 'COLLARS',
                'types' => ['COLLARS'],
            ],
            [
                'name' => 'DISC-TOP',
                'types' => ['DISC-TOP'],
            ],
            [
                'name' => 'DISPENSER PUMP',
                'types' => ['DISPENSER PUMP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'DROPPER',
                'types' => ['DROPPER'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'FLIP-TOP',
                'types' => ['FLIP-TOP'],
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'GLOSS ACCESSORIES',
                'types' => ['GLOSS ACCESSORIES'],
            ],
            [
                'name' => 'LIPSTICK',
                'types' => ['LIPSTICK'],
            ],
            [
                'name' => 'LOTION PUMP',
                'types' => ['LOTION PUMP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'LOTION PUMP + OC',
                'types' => ['LOTION PUMP + OC'],
            ],
            [
                'name' => 'LOTION SPRAY PUMP + OC',
                'types' => ['LOTION SPRAY PUMP + OC'],
            ],
            [
                'name' => 'OVERCAP SPRAY',
                'types' => ['OVERCAP SPRAY'],
            ],

            [
                'name' => 'OVERCAP LP',
                'types' => ['OVERCAP LP'],
            ],
            [
                'name' => 'OVERCAP LOTION',
                'types' => ['OVERCAP LOTION'],
            ],

            [
                'name' => 'OVERCAPS NAIL POLISH',
                'types' => ['OVERCAPS NAIL POLISH'],
            ],
            [
                'name' => 'NAIL POLISH CAPS',
                'types' => ['NAIL POLISH CAPS'],
            ],
            [
                'name' => 'NAIL POLISH OVERCAPS',
                'types' => ['NAIL POLISH OVERCAPS'],
            ],
            [
                'name' => 'NAIL POLISH BRUSH',
                'types' => ['NAIL POLISH BRUSH'],
            ],
            [
                'name' => 'FOAMER PUMP',
                'types' => ['FOAMER PUMP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'POWDER PUMP',
                'types' => ['POWDER PUMP'],
            ],
            [
                'name' => 'PUMP OVERCAP',
                'types' => ['PUMP OVERCAP'],
            ],
            [
                'name' => 'ROLL-ON',
                'types' => ['ROLL-ON'],
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'SPRAY PUMP + OC',
                'types' => ['SPRAY PUMP + OC'],
            ],
            [
                'name' => 'SPRAY PUMP',
                'types' => ['SPRAY PUMP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'SHIVE',
                'types' => ['SHIVE'],
            ],
            [
                'name' => 'STICKS',
                'types' => ['STICKS'],
            ],
            [
                'name' => 'SPONGE',
                'types' => ['SPONGE'],
            ],
            [
                'name' => 'PLUG',
                'types' => ['PLUG'],
            ],
            [
                'name' => 'TRIGGER',
                'types' => ['TRIGGER'],
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'WIPER',
                'types' => ['WIPER'],
            ],
        ],
        Compatibility::WORLD_NAIL_POLISH => [
            [
                'name' => 'BOTTLE',
                'types' => ['BOTTLE'],
            ],
            [
                'name' => 'GLASS BOTTLE',
                'types' => ['GLASS BOTTLE'],
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'NAIL POLISH REMOVER',
                'types' => ['NAIL POLISH REMOVER'],
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'BRUSHES',
                'types' => ['BRUSHES'],
            ],
            [
                'name' => 'CAP',
                'types' => ['CAP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'CAPS',
                'types' => ['CAPS'],
            ],
            [
                'name' => 'COLLARS',
                'types' => ['COLLARS'],
            ],
            [
                'name' => 'DISC-TOP',
                'types' => ['DISC-TOP'],
            ],
            [
                'name' => 'DISPENSER PUMP',
                'types' => ['DISPENSER PUMP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'DROPPER',
                'types' => ['DROPPER'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'FLIP-TOP',
                'types' => ['FLIP-TOP'],
            ],
            [
                'name' => 'GLOSS ACCESSORIES',
                'types' => ['GLOSS ACCESSORIES'],
            ],
            [
                'name' => 'LIPSTICK',
                'types' => ['LIPSTICK'],
            ],
            [
                'name' => 'LOTION PUMP',
                'types' => ['LOTION PUMP'],
            ],
            [
                'name' => 'LOTION PUMP + OC',
                'types' => ['LOTION PUMP + OC'],
            ],
            [
                'name' => 'LOTION SPRAY PUMP + OC',
                'types' => ['LOTION SPRAY PUMP + OC'],
            ],
            [
                'name' => 'OVERCAP SPRAY',
                'types' => ['OVERCAP SPRAY'],
            ],

            [
                'name' => 'OVERCAP LP',
                'types' => ['OVERCAP LP'],
            ],
            [
                'name' => 'OVERCAP LOTION',
                'types' => ['OVERCAP LOTION'],
            ],

            [
                'name' => 'OVERCAPS NAIL POLISH',
                'types' => ['OVERCAPS NAIL POLISH'],
            ],
            [
                'name' => 'NAIL POLISH CAPS',
                'types' => ['NAIL POLISH CAPS'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'NAIL POLISH OVERCAPS',
                'types' => ['NAIL POLISH OVERCAPS'],
            ],
            [
                'name' => 'NAIL POLISH BRUSH',
                'types' => ['NAIL POLISH BRUSH'],
            ],
            [
                'name' => 'FOAMER PUMP',
                'types' => ['FOAMER PUMP'],
            ],
            [
                'name' => 'POWDER PUMP',
                'types' => ['POWDER PUMP'],
            ],
            [
                'name' => 'PUMP OVERCAP',
                'types' => ['PUMP OVERCAP'],
            ],
            [
                'name' => 'SPRAY PUMP + OC',
                'types' => ['SPRAY PUMP + OC'],
            ],
            [
                'name' => 'SPRAY PUMP',
                'types' => ['SPRAY PUMP'],
            ],
            [
                'name' => 'SHIVE',
                'types' => ['SHIVE'],
            ],
            [
                'name' => 'STICKS',
                'types' => ['STICKS'],
            ],
            [
                'name' => 'PLUG',
                'types' => ['PLUG'],
            ],
            [
                'name' => 'SPONGE',
                'types' => ['SPONGE'],
            ],
            [
                'name' => 'TRIGGER',
                'types' => ['TRIGGER'],
            ],
            [
                'name' => 'WIPER',
                'types' => ['WIPER'],
            ],
        ],

        Compatibility::WORLD_PERFUMERY => [
            [
                'name' => 'BOTTLE',
                'types' => ['BOTTLE'],
            ],
            [
                'name' => 'GLASS BOTTLE',
                'types' => ['GLASS BOTTLE'],
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'PLASTIC BOTTLE',
                'types' => ['PLASTIC BOTTLE'],
            ],
            [
                'name' => 'CAP',
                'types' => ['CAP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'SPRAY PUMP',
                'types' => ['SPRAY PUMP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'SAMPLING',
                'types' => ['SAMPLING'],
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'NAIL POLISH CAPS',
                'types' => ['NAIL POLISH CAPS'],
            ],
            [
                'name' => 'NAIL POLISH OVERCAPS',
                'types' => ['NAIL POLISH OVERCAPS'],
            ],
            [
                'name' => 'NAIL POLISH BRUSH',
                'types' => ['NAIL POLISH BRUSH'],
            ],

        ],
        Compatibility::WORLD_HOME_FRAGRANCES => [
            [
                'name' => 'BOTTLE',
                'types' => ['BOTTLE'],
            ],
            [
                'name' => 'GLASS BOTTLE',
                'types' => ['GLASS BOTTLE'],
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'PLASTIC BOTTLE',
                'types' => ['PLASTIC BOTTLE'],
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'BRUSHES',
                'types' => ['BRUSHES'],
            ],
            [
                'name' => 'CAP',
                'types' => ['CAP'],
                'aligned_to' => 'top',
            ],
            [
                'name' => 'CAPS',
                'types' => ['CAPS'],
            ],
            [
                'name' => 'COLLARS',
                'types' => ['COLLARS'],
                'aligned_to' => 'bottom',
            ],
            [
                'name' => 'DISC-TOP',
                'types' => ['DISC-TOP'],
            ],
            [
                'name' => 'DISPENSER PUMP',
                'types' => ['DISPENSER PUMP'],
            ],
            [
                'name' => 'DROPPER',
                'types' => ['DROPPER'],
            ],
            [
                'name' => 'FLIP-TOP',
                'types' => ['FLIP-TOP'],
            ],
            [
                'name' => 'GLOSS ACCESSORIES',
                'types' => ['GLOSS ACCESSORIES'],
            ],
            [
                'name' => 'LIPSTICK',
                'types' => ['LIPSTICK'],
            ],
            [
                'name' => 'LOTION PUMP',
                'types' => ['LOTION PUMP'],
            ],
            [
                'name' => 'LOTION PUMP + OC',
                'types' => ['LOTION PUMP + OC'],
            ],
            [
                'name' => 'LOTION SPRAY PUMP + OC',
                'types' => ['LOTION SPRAY PUMP + OC'],
            ],
            [
                'name' => 'OVERCAP SPRAY',
                'types' => ['OVERCAP SPRAY'],
            ],

            [
                'name' => 'OVERCAP LP',
                'types' => ['OVERCAP LP'],
            ],
            [
                'name' => 'OVERCAP LOTION',
                'types' => ['OVERCAP LOTION'],
            ],

            [
                'name' => 'OVERCAPS NAIL POLISH',
                'types' => ['OVERCAPS NAIL POLISH'],
            ],
            [
                'name' => 'NAIL POLISH CAPS',
                'types' => ['NAIL POLISH CAPS'],
            ],
            [
                'name' => 'NAIL POLISH OVERCAPS',
                'types' => ['NAIL POLISH OVERCAPS'],
            ],
            [
                'name' => 'NAIL POLISH BRUSH',
                'types' => ['NAIL POLISH BRUSH'],
            ],
            [
                'name' => 'FOAMER PUMP',
                'types' => ['FOAMER PUMP'],
            ],
            [
                'name' => 'POWDER PUMP',
                'types' => ['POWDER PUMP'],
            ],
            [
                'name' => 'PUMP OVERCAP',
                'types' => ['PUMP OVERCAP'],
            ],
            [
                'name' => 'SPRAY PUMP + OC',
                'types' => ['SPRAY PUMP + OC'],
            ],
            [
                'name' => 'SPRAY PUMP',
                'types' => ['SPRAY PUMP'],
            ],
            [
                'name' => 'SHIVE',
                'types' => ['SHIVE'],
            ],
            [
                'name' => 'STICKS',
                'types' => ['STICKS'],
                'aligned_to' => 'middle',
            ],
            [
                'name' => 'SEALING CAPS',
                'types' => ['SEALING CAPS'],
            ],
            [
                'name' => 'PLUG',
                'types' => ['PLUG'],
            ],
            [
                'name' => 'SPONGE',
                'types' => ['SPONGE'],
            ],
            [
                'name' => 'TRIGGER',
                'types' => ['TRIGGER'],
            ],
            [
                'name' => 'WIPER',
                'types' => ['WIPER'],
            ],
        ],
        Compatibility::WORLD_CAPS_FOR_ALU_TUBES => [],
    ],

    'attributes' => [
        'BOTTLE' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',

        ],
        'PLASTIC BOTTLE' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'suggested_lps' => 'LPS',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
            'green_option' => 'GREEN OPTION',

        ],
        'GLASS BOTTLE' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'suggested_lps' => 'LPS',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',

        ],
        'NAIL POLISH REMOVER' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',

        ],
        'SAMPLING' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',

        ],
        'JAR' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',

        ],
        'PLASTIC JAR' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',

        ],
        'GLASS JAR' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',

        ],
        'ROLL-ON' => [
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',

        ],
        'AIRLESS BOTTLE' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'AIRLESS JAR' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',

        ],
        'AIRLESS JAR' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],

        'LIP GLOSS / CONCEALER' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],

        'LIPSTICK' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],


        'DROPPER' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'WIPER' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'SPRAY PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'OVERCAP SPRAY' => [
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'LOTION PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'OVERCAP LOTION' => [
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'SPRAY PUMP + OC' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'LOTION SPRAY PUMP + OC' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'CAPS' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
            'green_option' => 'GREEN OPTION',
        ],
        'OVERCAPS NAIL POLISH' => [
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'REF. CAPS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'NAIL POLISH CAPS' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
            'green' => 'GREEN OPTION',
        ],
        'NAIL POLISH OVERCAPS' => [
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'REF. CAPS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'NAIL POLISH BRUSH' => [
            'attrib_1' => 'MISURA TESTA ASTINA',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'REF. CAPS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'BRUSHES' => [
            'attrib_1' => 'MISURA TESTA ASTINA',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'REF. CAPS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'COLLARS' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_8' => 'NOTE',
        ],
        'STICKS' => [
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_8' => 'NOTE',
        ],
        'SEALING CAPS' => [
            'attrib_2' => 'MATERIALS',
        ],
        'SPONGE' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
        ],

        'PLUG' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
        ],


        'FLIP-TOP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_8' => 'NOTE',
        ],
        'DISC-TOP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_8' => 'NOTE',
        ],
        'DISPENSER PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_8' => 'NOTE',
        ],
        'TRIGGER' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'SHIVE' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_8' => 'NOTE',
        ],
        'FOAMER PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_4' => 'FINISHING',
            'attrib_8' => 'NOTE',
        ],
        'POWDER PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_4' => 'FINISHING',
            'attrib_8' => 'NOTE',
        ],

        'CAP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],

        'PUMP OVERCAP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],

        'GLOSS ACCESSORIES' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],

        'LIPSTICK' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],

        'DISPENSER PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
    ],

    /**
     * Permette di gestire le varia customizzazioni dei filtri
     * In esempio:
     * - multiple: rende la select a più opzioni
     */
    'options' => [
        'SIZE' => [
            'multiple' => true,
        ],
        'MATERIALS' => [
            'multiple' => true,
        ],
        'GREEN OPTION' => [
            'checkbox' => ['ON REQUEST', 'AVAILABLE'],
        ],
    ],

    /**
     * Permette di prefissare dei filtri per escludere i prodotti
     */
    'excludes' => [
        'PACK' => [
            'attrib_4' => ['FROSTED',],
        ],
    ],

    // The order here decides in wich order the filters will displayed in the configurator
    'filters' => [
        'BOTTLE' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'GLASS BOTTLE' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'suggested_lps' => 'LPS',
            'attrib_6' => 'SHAPE',
        ],
        'NAIL POLISH REMOVER' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'SAMPLING' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'PLASTIC BOTTLE' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'suggested_lps' => 'LPS',
            'attrib_6' => 'SHAPE',
            'green_option' => 'GREEN OPTION',
        ],
        'JAR' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'GLASS JAR' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'PLASTIC JAR' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'ROLL-ON' => [
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'AIRLESS BOTTLE' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'AIRLESS JAR' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'AIRLESS JAR' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',

        ],

        'LIP GLOSS / CONCEALER' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],


        'DROPPER' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'WIPER' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_6' => 'SHAPE',
        ],
        'SPRAY PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'OVERCAP SPRAY' => [
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'LOTION PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'OVERCAP LOTION' => [
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'OVERCAP LP' => [
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'SPRAY PUMP + OC' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'LOTION PUMP + OC' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'CAPS' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'OVERCAPS NAIL POLISH' => [
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'REF. CAPS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'NAIL POLISH CAPS' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
            'green_option' => 'GREEN OPTION',
        ],
        'NAIL POLISH OVERCAPS' => [
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'REF. CAPS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'NAIL POLISH BRUSH' => [
            'attrib_1' => 'MISURA TESTA ASTINA',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'REF. CAPS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'attrib_8' => 'NOTE',
        ],
        'BRUSHES' => [
            'attrib_1' => 'MISURA TESTA ASTINA',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'REF. CAPS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'COLLARS' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
        ],
        'STICKS' => [
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
        ],
        'SEALING CAPS' => [
            'attrib_2' => 'MATERIALS',
        ],
        'SPONGE' => [
            'attrib_2' => 'MATERIALS',
        ],
        'PLUG' => [
            'attrib_2' => 'MATERIALS',
        ],
        'FLIP-TOP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
        ],
        'DISC-TOP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
        ],
        'DISPENSER PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
        ],
        'TRIGGER' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
        'SHIVE' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
        ],
        'FOAMER PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_4' => 'FINISHING',
        ],
        'POWDER PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_4' => 'FINISHING',
        ],
        'FOAMER PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_4' => 'FINISHING',
        ],
        'GLOSS/LIP BRUSH' => [
            'attrib_1' => 'NECK',
            'attrib_4' => 'FINISHING',
        ],

        'CAP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
            'green_option' => 'GREEN OPTION',
        ],

        'PUMP OVERCAP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],

        'GLOSS ACCESSORIES' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],

        'LIPSTICK' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],

        'DISPENSER PUMP' => [
            'attrib_1' => 'NECK',
            'attrib_2' => 'MATERIALS',
            'attrib_3' => 'SIZE',
            'attrib_4' => 'FINISHING',
            'attrib_6' => 'SHAPE',
        ],
    ],
];
