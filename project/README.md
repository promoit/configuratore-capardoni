# Capardoni

## Import dei prodotti

### Backup
Accedere a `Polli` e caricare dentro la cartella `Progetti\capardoni\Configuratore` il file.

### Admin
Accedere all'admin di demo e caricare dall'apposita sezione.
Se andrà tutto a posto, allora procedere a caricarlo dall'admin in produzione.
Una volta terminata l'import in produzione, creare un foglio vuoto di Excel e fare copia-incolla della lista delle foto mancanti.

## Foto

Il cliente carica solo le foto sull'FTP di demo.
Quando ci chiederà di caricare le foto, basterà scaricarle e ricaricarle nella cartella in produzione, sostituendo i file identici.

Una volta terminato l'upload, connettersi in SSH e lanciare, dentro la cartella `project` il seguente comando e lasciare che il sistema rigeneri le thumbnail delle foto aggiornate (viene controllato il loro HASH) e registri le dimensione del foto dentro i PNG

```bash
php artisan run:generate-thumbnail
```
