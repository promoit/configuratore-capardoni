<?php

return [
    'apikey' => env('OPIDEM_APIKEY', null),
    'entrypoint' => env('OPIDEM_ENTRYPOINT', null),
    'recipient' => env('OPIDEM_RECIPIENT', null),
];
