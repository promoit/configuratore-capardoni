<?php

return [

    'folder' => storage_path('app/supervisor'),

    'commands' => [

        'queue' => [
            'command' => 'queue:work',
            'params' => [],
        ],
    ],

];
