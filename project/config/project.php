<?php

return [

    'version' => '1.2.1',
    'asset_version' => env('ASSET_VERSION', '1.0.0'),

    'leads' => [
        'addresses' => explode(';', env('MAIL_TO_ADDRESS', '')),
        'bcc' => explode(';', env('MAIL_BCC_ADDRESS', '')),
    ],

    'messages' => [
        'save' => [
            'success' => 'Thank you for your interest in our products. We have sent you a link to quickly return to this configuration.',
            'failure' => 'Sorry there was an error, please retry.',
        ],
        'quote' => [
            'success' => 'Thank you for your interest in our products. Our Sales Department will contact you as soon as possible.',
            'failure' => 'Sorry there was an error, please retry.',
        ],
        'download' => [
            'success' => 'Thank you for your interest in our products. We have sent you an attachment of this configuration.',
            'failure' => 'Sorry there was an error, please retry.',
        ],
    ],

    'email' => [
        'from' => [
            'address' => env('MAIL_FROM_ADDRESS'),
            'name' => env('MAIL_FROM_NAME'),
        ],
    ],

    'google' => [
        'recaptha' => [
            'v3' => [
                'public' => env('RECAPTCHA_PUBLIC_KEY'),
                'private' => env('RECAPTCHA_PRIVATE_KEY'),
            ],
        ],
        'tag_manager' => env('GOOGLE_TAG_MANAGER'),
    ],

    'support' => [
        'email' => 'digital@opiquad.it'
    ],
];
