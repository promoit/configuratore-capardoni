<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompatibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compatibilities', function (Blueprint $table) {
            $table->id();
            $table->boolean('skincare');
            $table->boolean('personal_care');
            $table->boolean('nail_polish');
            $table->boolean('make_up');
            $table->boolean('perfumery');
            $table->boolean('home_fragrances');
            $table->text('title');
            $table->string('code', 50);
            $table->string('image', 50);
            $table->timestamps();

            $table->index(['skincare']);
            $table->index(['personal_care']);
            $table->index(['nail_polish']);
            $table->index(['make_up']);
            $table->index(['perfumery']);
            $table->index(['home_fragrances']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compatibilities');
    }
}
