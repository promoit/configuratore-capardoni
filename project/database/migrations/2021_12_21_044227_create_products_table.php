<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('type', 25);
            $table->string('image', 50);
            $table->string('capardoni_code')->nullable();
            $table->string('web_code', 25);
            $table->text('name');
            $table->decimal('height', 10, 2)->nullable();
            $table->decimal('length', 10, 2)->nullable();
            $table->decimal('width', 10, 2)->nullable();
            $table->decimal('diameter', 10, 2)->nullable();
            $table->decimal('suggested_lps', 10, 2)->nullable();
            $table->decimal('weight', 10, 2)->nullable();
            $table->decimal('dosage', 10, 2)->nullable();
            $table->string('green')->nullable();
            $table->string('attrib_1')->nullable();
            $table->string('attrib_2')->nullable();
            $table->string('attrib_3')->nullable();
            $table->string('attrib_4')->nullable();
            $table->string('attrib_5')->nullable();
            $table->string('attrib_6')->nullable();
            $table->string('attrib_7')->nullable();
            $table->string('attrib_8')->nullable();
            $table->timestamps();

            $table->index(['web_code']);
            $table->index(['attrib_1']);
            $table->index(['attrib_2']);
            $table->index(['attrib_3']);
            $table->index(['attrib_4']);
            $table->index(['attrib_5']);
            $table->index(['attrib_6']);
            $table->index(['attrib_7']);
            $table->index(['attrib_8']);

            $table->index(['web_code', 'attrib_1']);
            $table->index(['web_code', 'attrib_2']);
            $table->index(['web_code', 'attrib_3']);
            $table->index(['web_code', 'attrib_4']);
            $table->index(['web_code', 'attrib_5']);
            $table->index(['web_code', 'attrib_6']);
            $table->index(['web_code', 'attrib_7']);
            $table->index(['web_code', 'attrib_8']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
