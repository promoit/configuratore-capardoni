<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCompatibilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_compatibility', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained('products');
            $table->foreignId('compatibility_id')->constrained('compatibilities');
            $table->string('type', 25);
            $table->timestamps();

            $table->index(['type', 'compatibility_id', 'product_id'], 'search_compatibility');
            $table->index(['type', 'product_id', 'compatibility_id'], 'search_product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_compatibility');
    }
}
