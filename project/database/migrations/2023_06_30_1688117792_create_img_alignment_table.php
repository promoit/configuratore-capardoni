<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImgAlignmentTable extends Migration
{
    public function up()
    {
        Schema::create('img_alignment', function (Blueprint $table) {

		$table->integer('id',11);
		$table->char('img',100);
		$table->char('checksum',100);
		$table->float('w');
		$table->float('h');
		$table->float('yt');
		$table->float('yb');

        $table->index('img');
        $table->index('checksum');

        });
    }

    public function down()
    {
        Schema::dropIfExists('img_alignment');
    }
}
