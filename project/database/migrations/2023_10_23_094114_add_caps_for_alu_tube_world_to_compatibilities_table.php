<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCapsForAluTubeWorldToCompatibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compatibilities', function (Blueprint $table) {
            $table->boolean('caps_for_alu_tubes')
                ->default(true)
                ->after('home_fragrances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compatibilities', function (Blueprint $table) {
            $table->dropColumn('caps_for_alu_tubes');
        });
    }
}
