function deleteResource(url) {
    let token = document.head.querySelector('meta[name="csrf-token"]');
    $('<form method="post" action="' + url + '">' +
      '<input type="hidden" name="_token" value="' + token.content + '">' +
      '<input type="hidden" name="_method" value="delete">' +
      '</form>').appendTo('body').submit();
}

function confirmDelete(message, url) {

    swal({
        title: 'ATTENZIONE!',
        confirmButtonColor: '#fb6340',
        type: 'error',
        text: message,
        showCancelButton: true,
        focusCancel: true,
        confirmButtonText: 'Si, cancella!',
        cancelButtonText: 'Annulla'
    })
    .then(function (result) {
        if (result.value) {
            deleteResource(url);
        }
    })
    .catch(swal.noop);

}  

function confirmBulkDelete(message, form) {

    swal({
        title: 'ATTENZIONE!',
        confirmButtonColor: '#fb6340',
        type: 'error',
        text: message,
        showCancelButton: true,
        focusCancel: true,
        confirmButtonText: 'Si, cancella!',
        cancelButtonText: 'Annulla'
    })
        .then(function (result) {
            if (result.value) {
                $(form).submit();
            }
        })
        .catch(swal.noop);

}  

$(function () {

    $('.action-delete').click(function(event) {
        event.preventDefault();
        var url = $(this).data('url');
        var message = $(this).data('message');
        confirmDelete(message, url);
    });

    $('.action-bulk-delete').click(function (event) {
        event.preventDefault();
        var message = $(this).data('message');
        var form = $(this).data('form');
        confirmBulkDelete(message, form);
    });

    $('.sortable').sortable({
        helper: function (e, ui) {
            ui.children().each(function () {
                $(this).width($(this).width());
            });
            return ui;
        }
    }).disableSelection();
});
