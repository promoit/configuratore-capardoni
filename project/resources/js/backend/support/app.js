/**
 * app.js
 * Javascript library to handle WillEasy backend
 *
 * @version 1.0
 * @author  Filippo Toso, https://github.com/filippotoso/
 * @updated 2019-05-22
 * @link    https://www.buonipasto.it/
 *
 *
 */
window.App = (function () {

    var imagePreview = function (upload, image) {

        jQuery(function ($) {

            $(upload).change(function () {

                if (this.files && this.files[0]) {

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(image).attr('src', e.target.result);
                    }

                    reader.readAsDataURL(this.files[0]);

                }

            });

        });

    };

    var filePreview = function (upload, label, callback) {

        jQuery(function ($) {

            $(upload).change(function () {
                if (callback instanceof Function) {
                    callback(this.files[0].name);
                } else {
                    $(label).html(this.files[0].name);
                }
            });

        });

    };

    var locationMarker = function (locationMap) {

        var myLatlng = new google.maps.LatLng(locationMap.lat, locationMap.lng);
        var mapOptions = {
            zoom: 16,
            center: myLatlng
        }
        locationMap.map = new google.maps.Map(document.getElementById('location_map'), mapOptions);

        // Place a draggable marker on the map
        locationMap.marker = new google.maps.Marker({
            position: myLatlng,
            map: locationMap.map,
            draggable: true,
            title: 'Trascina il puntatore nella mappa per il punto corretto'
        });

        locationMap.marker.addListener('dragend', function (event) {
            document.getElementById('latitude').value = event.latLng.lat();
            document.getElementById('longitude').value = event.latLng.lng();
        });

    };


    var disableSubmitOnReturn = function (el) {

        el = (el === undefined) ? 'form' : el;

        jQuery(function ($) {

            $(el).on('keyup keypress', function (e) {
                var keyCode = e.keyCode || e.which;
                if ((keyCode === 13) && !$(e.target).is("textarea")) {
                    e.preventDefault();
                    return false;
                }
            });

        });

    }

    var autocompleteLocation = function (locationId, latitudeId, longitudeId) {

        locationId = (typeof locationId !== 'undefined') ? locationId : 'location_autocomplete';
        latitudeId = (typeof latitudeId !== 'undefined') ? latitudeId : 'latitude';
        longitudeId = (typeof longitudeId !== 'undefined') ? longitudeId : 'longitude';

        var components = {
            street_number: { id: 'short_name', field: 'street_number' },
            route: { id: 'long_name', field: 'address' },
            locality: { id: 'long_name', field: 'city' },
            administrative_area_level_2: { id: 'short_name', field: 'province' },
            postal_code: { id: 'short_name', field: 'zip' },
        };

        var autocomplete = new google.maps.places.Autocomplete(
            document.getElementById(locationId), {
            types: ['geocode']
        });

        autocomplete.addListener('place_changed', function () {

            var place = autocomplete.getPlace();

            document.getElementById(latitudeId).value = place.geometry.location.lat();
            document.getElementById(longitudeId).value = place.geometry.location.lng();

            if (typeof updateLocation == 'function') {
                updateLocation(place.geometry.location.lat(), place.geometry.location.lng());
            }

            for (var component in components) {
                var el = document.getElementById(component);
                if (el) {
                    el.value = '';
                    el.disabled = false;
                }
            }

            if (place.address_components) {
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (components[addressType]) {
                        var val = place.address_components[i][components[addressType]['id']];
                        var el = document.getElementById(components[addressType]['field']);
                        if (el) {
                            el.value = val;
                        }

                        if (addressType == 'administrative_area_level_2') {
                            val = place.address_components[i]['long_name'];
                            el.value = val.replace('Provincia di ', '');
                        }
                    }


                }

                var address = '';

                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (components[addressType]) {
                        if (addressType == 'route') {
                            address = place.address_components[i]['long_name'];
                        }
                    }
                }

                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (components[addressType]) {
                        if (addressType == 'street_number') {
                            address += ' ' + place.address_components[i]['short_name'];
                        }
                    }
                }

                document.getElementById('address').value = address;

            }

        });

    };

    var copyToClipboard = function(text) {
        var textArea = document.createElement("textarea");

        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;

        textArea.style.width = '2em';
        textArea.style.height = '2em';

        textArea.style.padding = 0;

        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';

        textArea.style.background = 'transparent';

        textArea.value = text;
        document.body.appendChild(textArea);

        textArea.select();

        try {
            var successful = document.execCommand('copy');
        } catch (err) {
        }
        document.body.removeChild(textArea);
    };

    var slugify = function (string) {
        const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
        const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnooooooooprrsssssttuuuuuuuuuwxyyzzz------'
        const p = new RegExp(a.split('').join('|'), 'g')

        return string.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
            .replace(/&/g, '-and-') // Replace & with 'and'
            .replace(/[^\w\-]+/g, '') // Remove all non-word characters
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, '') // Trim - from end of text
    };

    // Return properties and methods
    return {
        imagePreview: imagePreview,
        filePreview: filePreview,
        locationMarker: locationMarker,
        disableSubmitOnReturn: disableSubmitOnReturn,
        autocompleteLocation: autocompleteLocation,
        copyToClipboard: copyToClipboard,
        slugify: slugify
    }

})();
