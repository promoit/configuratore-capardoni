import Vue from "vue";
import vSelect from "vue-select";
import { VueReCaptcha } from "vue-recaptcha-v3";

Vue.use(VueReCaptcha, {
    siteKey: process.env.MIX_RECAPTCHA_KEY,
    loaderOptions: {
        useRecaptchaNet: true,
        explicitRenderParameters: {
            badge: "bottomright",
            size: "invisible",
        },
    },
});

Vue.component("v-select", vSelect);

Vue.component(
    "svg-viewer",
    require("./configurator/support/SvgViewer.vue").default
);
Vue.component(
    "lazy-image",
    require("./configurator/support/LazyImage.vue").default
);

Vue.component(
    "category-selector",
    require("./configurator/category-selector/CategorySelector.vue").default
);

Vue.component(
    "selection-viewer",
    require("./configurator/selection-viewer/SelectionViewer.vue").default
);
Vue.component(
    "product-viewer",
    require("./configurator/selection-viewer/ProductViewer.vue").default
);
Vue.component(
    "combination-viewer",
    require("./configurator/selection-viewer/CombinationViewer.vue").default
);

Vue.component(
    "configurator-header",
    require("./configurator/layout/ConfiguratorHeader.vue").default
);
Vue.component(
    "configurator-footer",
    require("./configurator/layout/ConfiguratorFooter.vue").default
);
Vue.component(
    "configurator",
    require("./configurator/Configurator.vue").default
);

Vue.component(
    "quote-popup",
    require("./configurator/popups/QuotePopup.vue").default
);
Vue.component(
    "save-popup",
    require("./configurator/popups/SavePopup.vue").default
);
Vue.component(
    "download-popup",
    require("./configurator/popups/DetailsProductPopup.vue").default
);
Vue.component(
    "message-popup",
    require("./configurator/popups/MessagePopup.vue").default
);

Vue.config.devtools = true;

window.Vue = Vue;
