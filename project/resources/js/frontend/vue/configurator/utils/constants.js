export const WORLD_LABELS = {
    "make-up": "Make-up",
    "personal-care": "Personal Care",
    "nail-polish": "Nail Polish",
    "make-up-skincare": "Make-up & Skincare",
    "perfumery": "Perfumery",
    "home-fragrances": "Home Fragrances",
};

export const EVENT_SAVE = 'save';
export const EVENT_QUOTE = 'quote';
export const EVENT_DETAILS = 'details';
export const EVENT_RESTART = 'restart';
export const EVENT_SELECT = 'select';
export const EVENT_UNSELECT = 'unselect';
export const EVENT_PREVIEW = 'preview';
export const EVENT_SCROLL = 'scroll';
export const EVENT_MESSAGE = 'message';

export const SAVE_PRIVACY_LINK = 'https://www.capardoni.com/en/privacy-configurator/';
export const QUOTE_PRIVACY_LINK = 'https://www.capardoni.com/en/privacy-policy-en/';
export const DETAILS_PRIVACY_LINK = 'https://www.capardoni.com/en/privacy-configurator/';

export const PRIVACY_LINK = 'https://www.capardoni.com/en/privacy-policy-en/';
export const COOKIE_LINK = 'https://www.capardoni.com/en/cookie-policy-en/';
export const COMPANY_LINK = 'https://www.capardoni.com/en/contact/#companyinfo';
