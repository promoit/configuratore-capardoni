@component('mail::message', ['title' => 'Congratulazioni!'])
# Il tuo account è pronto

Ciao,

abbiamo appena creato il tuo utente. Ecco i tuoi dati di accesso:

Username: {{ $user->email }}<br>
Password: {{ $password }}

@component('mail::button', ['url' => route('login', ['email' => $user->email])])
Accedi alla Piattaforma
@endcomponent

A presto,<br>
{{ config('app.name') }}
@endcomponent