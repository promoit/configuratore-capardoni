@extends('layouts.mail')

@section('content')

    <p>Thanks for your interest in our products.
Attached you can find the details of the selected configuration.
We are at your disposal for any info you may need.
Thanks from the Capardoni team!
</p>
<br/>
<br/>
<em><small>The information declared in this catalogue are approximate and we reserve the right to make changes without prior notice.</small></em>
@endsection