@extends('layouts.mail')

@section('content')

    <p>Hai ricevuto la seguente richiesta di preventivo:</p>

    <ul>
        <li>Url: <a href="{{ $lead->url }}">{{ $lead->url }}</a></li>
        <li>Categoria: {{ $lead->world }}</li>
        <li>Nome: {{ $lead->firstname }} {{ $lead->lastname }}</li>
        <li>Azienda: {{ $lead->company }}</li>
        <li>Telefono: {{ $lead->phone }}</li>
        <li>Email: {{ $lead->email }}</li>
        <li>Paese: {{ $lead->country }}</li>
        <li>Privacy: {{ $lead->privacy_at->format('d/m/Y H:i:s') }}</li>
        <li>Marketing: {{ $lead->marketing_at ? $lead->marketing_at->format('d/m/Y H:i:s') : 'non accettato' }}</li>
    </ul>

    @if ($lead->message)
        <p>L'utente ha incluso il seguente messaggio:</p>
        <p><i>{{ $lead->message }}</i></p>
    @endif

    <p>A presto!</p>
@endsection

