@extends('layouts.mail')

@section('content')
    <h1 style="color: #001f69; font-family: Verdana, sans-serif; font-size: 28px; font-style: normal; font-weight: bold; text-align: left; text-transform: none;">Hello,<br><br>thank you for using Capardoni's Full Pack Configurator service</h1>

    <p>Below you will find the link to take you back to the configuration you have selected.</p>

    <p><a href="{{ $lead->url }}" style="color: #7892dc; font-style: normal; font-weight: bold; text-decoration: none; text-transform: none;">{{ route('frontend.configurator.index') }}</a></p>

    <p>We remind you that we will be happy to help you if you have any questions about our products or customizations, or to request a free quote. </p>

    <ul>
        <li>Full name: {{ $lead->firstname }} {{ $lead->lastname }}</li>
        <li>Company: {{ $lead->company }}</li>
        <li>Email: {{ $lead->email }}</li>
        <li>Country: {{ $lead->country }}</li>
    </ul>

    <p>Thanks from the Capardoni team</p>
    <p>&nbsp;</p>
    <small>The information declared in this catalogue are approximate and we reserve the right to make changes without prior notice.</small>
@endsection
