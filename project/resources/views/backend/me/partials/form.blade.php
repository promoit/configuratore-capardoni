<div class="pl-lg-4">
    <div class="row">
        <div class="col-lg-6">
            @text(['name' => 'name', 'value' => $user->name, 'label' => 'Nome', 'placeholder' => 'Mario Rossi'])                           
        </div>
        <div class="col-lg-6">
            @email(['name' => 'email', 'value' => $user->email, 'label' => 'Email', 'placeholder' => 'mario.rossi@example.com'])                           
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            @password(['name' => 'password', 'value' => '', 'label' => 'Password'])                           
        </div>
        <div class="col-lg-6">
            @password(['name' => 'password_confirmation', 'value' => '', 'label' => 'Conferma password'])                           
        </div>
    </div>
</div>