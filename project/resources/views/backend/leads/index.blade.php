@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="col">
            <div class="card shadow">

                <div class="table-responsive">

                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Url</th>
                                <th scope="col">World</th>
                                <th scope="col">Type</th>
                                <th scope="col">Firstname</th>
                                <th scope="col">Lastname</th>
                                <th scope="col">Company</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Country</th>
                                <th scope="col">Email</th>
                                <th scope="col">Message</th>
                                <th scope="col">Marketing_at</th>
                                <th scope="col">Created_at</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if ($leads->count() == 0)
                                <tr>
                                    <td class="text-center" colspan="5">
                                        <h3>Al momento non ci sono dati presenti.</h3>
                                    </td>
                                </tr>
                            @else

                                @foreach ($leads as $k => $lead)
                                <tr>
                                    <th scope="row">{{ $k+1 }}</td>
                                    <td><a href="{{ $lead->url }}" target="_blank">Guarda {{$page}}</a></td>
                                    <td>{{ $lead->world }}</td>
                                    <td>{{ $lead->type }}</td>
                                    <td>{{ $lead->firstname }}</td>
                                    <td>{{ $lead->lastname }}</td>
                                    <td>{{ $lead->company }}</td>
                                    <td>{{ $lead->phone }}</td>
                                    <td>{{ $lead->country }}</td>
                                    <td>{{ $lead->email }}</td>
                                    <td>{{ $lead->message }}</td>
                                    <td>{{ $lead->marketing_at }}</td>
                                    <td>{{ $lead->created_at }}</td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
@endsection
