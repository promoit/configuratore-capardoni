<form method="GET" action="{{ $searchRoute }}">
    <div class="row">
        <span class="col-xl-1 col-lg-2 col-md-3 col-form-label font-weight-bold">Cerca</span>
        <div class="col-xl-11 col-lg-10 col-md-9">
            <div class="row">
                <div class="col-xl-10 col-lg-9 col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control form-control-alternative" name="search" placeholder="Lorem ipsum..."  value="{{ $input['search'] ?? '' }}">
                    </div>
                </div>
                <div class="col">
                    @submit(['class' => 'btn-primary', 'label' => 'Cerca'])
                </div>
            </div>
        </div>
    </div>
</form>
