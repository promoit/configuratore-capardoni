@extends('layouts.backend')

@section('content')

    <div class="row">
        <div class="col">

            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Risultato importazione</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h6 class="heading-small text-muted mb-4">Informazioni sull'importazione</h6>
                    
                    <div class="row">
                        <div class="col">
                            @if ((count($productCodes) == 0) && (count($images) == 0))
                                <p>Importazione completata con successo.</p>
                            @else
                                <p>Importazione partiale completata.</p>
                                
                                <div class="row">
                                    @if (count($productCodes) > 0)
                                    <div class="col-6">
                                        <p>I seguenti codici prodotto presenti nel foglio COMPATIBILITY non sono stati trovati nel foglio ARTICOLI SINGOLI. Tutte le combinazioni che li includono NON sono state importate.</p>
                                        <p>
                                        @foreach ($productCodes as $productCode)    
                                            {{ $productCode }}<br>
                                        @endforeach
                                        </p>
                                    </div>
                                    @endif
                                    @if (count($images) > 0)
                                    <div class="col-6">
                                        <p>Le immagini dei seguenti prodotti e combinazioni non sono presenti:</p>
                                        <p>
                                        @foreach ($images as $image)    
                                            {{ $image }}<br>
                                        @endforeach
                                        </p>
                                    </div>
                                    @endif
                                </div>
                                
                                
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
@endsection