@extends('layouts.backend')

@section('content')

    <div class="row">
        <div class="col">

            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Utente {{ $user->name }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route($route . '.edit', ['user' => $user]) }}" class="btn btn-sm btn-highlight">Modifica</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h6 class="heading-small text-muted mb-4">Informazioni sull'utente</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-6">
                                @value(['label' => 'Nome', 'value' => $user->name])
                            </div>
                            <div class="col-lg-6">
                                @value(['label' => 'Nome', 'value' => $user->email])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
@endsection