<div class="pl-lg-4">
    <div class="row">
        <div class="col-lg-6">
            @text(['name' => 'name', 'value' => $user->name, 'label' => 'Nome', 'placeholder' => 'Mario Rossi'])                           
        </div>
        <div class="col-lg-6">
            @email(['name' => 'email', 'value' => $user->email, 'label' => 'Email', 'placeholder' => 'mario.rossi@example.com'])                           
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            @password(['name' => 'password', 'value' => '', 'label' => 'Password'])                           
        </div>
        <div class="col-lg-6">
            @password(['name' => 'password_confirmation', 'value' => '', 'label' => 'Conferma password'])                           
        </div>
        <div class="col-lg-12 text-right mb-4">
            <button class="btn btn-primarish action-password" type="button">Genera Password</button>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <label class="form-control-label">Ruoli</label>
        </div>
        @foreach($roles as $role => $name)
        <div class="col-3">
            <div class="form-group">
                <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" id="roles_{{ $role }}" class="custom-control-input" name="roles[{{ $role }}]" value="{{ $role }}" {{ $user->hasRole($role) ? 'checked' : '' }}> 
                    <label for="roles_{{ $role }}" class="custom-control-label">{{ $name }}</label>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @if ($action == 'create')
    <div class="row">
        <div class="col">
            @checkbox(['name' => 'notify', 'label' => 'Notifica via email', 'value' => 1])
        </div>
    </div>
    @endif

</div>

@push('footer')
<script>
    $(function () {
        $('.action-password').click(function(){
            var password = Math.random().toString(36).slice(2);
            $('#password').attr('type', 'text');
            $('#password_confirmation').attr('type', 'text');
            $('#password').val(password);
            $('#password_confirmation').val(password);
        });
    });
</script>
@endpush
