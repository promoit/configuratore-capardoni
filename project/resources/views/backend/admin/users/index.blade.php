@extends('layouts.backend')

@section('content')

    <div class="row">
        <div class="col">
            <div class="card shadow">
                        
                <div class="card-header border-0">
                    <div class="row">
                        <div class="col-8">
                            <h3 class="mb-0">Gestione Utenti</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route($route . '.create') }}" class="btn btn-sm btn-success">Crea Utente</a>
                        </div>
                    </div>
                </div>
                
                <div class="card-body">
                    @include('backend.common.search-bar', ['searchRoute' => route($route . '.index')])
                </div>

                <div class="table-responsive">

                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Email</th>
                                <th scope="col" class="text-right">Azioni</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if ($users->count() == 0)
                                <tr>
                                    <td class="text-center" colspan="5">
                                        <h3>Al momento non ci sono dati presenti.</h3>
                                    </td>
                                </tr>
                            @else
                                @foreach ($users as $current)
                                <tr>
                                    <th scope="row">{{ $current->id }}</td>
                                    <td>{{ $current->name }}</td>
                                    <td>{{ $current->email }}</td>
                                    <td class="text-right record-actions">
                                        <a class="text-info" href="{{ route($route . '.show', ['user' => $current]) }}" title="Visualizza">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a class="text-success" href="{{ route($route . '.clone', ['user' => $current]) }}" title="Duplica">
                                            <i class="fa fa-clone"></i>
                                        </a>
                                        <a class="text-yellow   " href="{{ route($route . '.edit', ['user' => $current]) }}" title="Modifica">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a class="text-warning" href="{{ route('backend.admin.users.impersonate', ['user' => $current]) }}" title="Impersona">
                                            <i class="fa fa-user"></i>
                                        </a>
                                        <a class="text-danger action-delete" href="#" data-message="Sei sicuro di voler cancellare questo utente?" data-url="{{ route($route . '.delete', ['user' => $current]) }}" title="Elimina">
                                            <i class="fa fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>

                @if ($users->count() > 0)
                <div class="card-footer py-4 text-xs-center">
                    <nav aria-label="...">
                        {{ $users->links() }}
                    </nav>
                </div>
                @endif

            </div>
        </div>
    </div>
@endsection