@extends('layouts.backend')

@section('content')

    <div class="row">
        <div class="col">

            <form method="POST" action="{{ route($route . '.update', ['user' => $user]) }}">
                @csrf
                @method('patch')

                @set('action', 'edit')

                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Modifica Utente</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">

                        <h6 class="heading-small text-muted mb-4">Informazioni sull'utente</h6>

                        @include('backend.admin.users.partials.form')

                        <div class="row">
                            <div class="col text-right">
                                @submit(['class' => 'btn-success', 'label' => 'Salva Utente'])
                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
    </div>
@endsection
