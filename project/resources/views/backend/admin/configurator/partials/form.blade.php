<div class="row">
    <div class="col-12">
        @textarea(['name' => 'content', 'label' => 'Configurazione', 'required' => true, 'value' => $content, 'rows' => 28, 'language' => 'php'])
        {{-- @wysiwyg(['name' => 'content', 'label' => 'Configurazione', 'required' => true, 'value' => $content, 'rows' => 28, 'language' => 'php']) --}}
    </div>
</div>
