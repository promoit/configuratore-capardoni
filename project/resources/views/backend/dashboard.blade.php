@extends('layouts.backend')

@section('content')

    <div class="row card-wrapper">
        @role(App\Models\Role::ADMIN)
        <div class="col-4">
            <div class="card shadow dashboard-card">
                <div class="card-body">
                    <h3 class="card-title mb-3">Gestione Utenti</h3>
                    <p class="card-text mb-4">Crea, modifica ed elimina gli utenti della piattaforma.</p>
                    <div class="text-center">
                        <a href="{{ route('backend.admin.users.index') }}" class="btn btn-primary btn-icon">
                            <span class="btn-inner--icon"><i class="fa fa-user"></i></span>
                            <span class="btn-inner--text">Accedi</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-4">
            <div class="card shadow dashboard-card">
                <div class="card-body">
                    <h3 class="card-title mb-3">Importazione Prodotti</h3>
                    <p class="card-text mb-4">Importa prodotti e combinazioni.</p>
                    <div class="text-center">
                        <a href="{{ route('backend.admin.products.edit') }}" class="btn btn-primary btn-icon">
                            <span class="btn-inner--icon"><i class="fa fa-box"></i></span>
                            <span class="btn-inner--text">Accedi</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-4">
            <div class="card shadow dashboard-card">
                <div class="card-body">
                    <h3 class="card-title mb-3">Configurazione</h3>
                    <p class="card-text mb-4">Configura categorie, filtri ed etichette.</p>
                    <div class="text-center">
                        <a href="{{ route('backend.admin.configurator.edit') }}" class="btn btn-primary btn-icon">
                            <span class="btn-inner--icon"><i class="fa fa-cog"></i></span>
                            <span class="btn-inner--text">Accedi</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endrole
    </div>

@endsection

