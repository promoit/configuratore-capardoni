@extends('layouts.frontend')

@section('content')

    @include($view . '.partials.header')

    <main id="main" class="is-home">
        <section id="worlds">
            <h2>Welcome to the<br />Capardoni Full Pack Configurator</h2>
            <p>Configure the packaging you need.<br />Select the product category to proceed.</p>

            <ul class="worlds">
                @foreach($worlds as $id => $world)
                <li class="world{{ $world['empty'] ? " is-empty" : "" }}">
                    <a href="{{ !$world['empty'] ? route($route . '.show', ['world' => $id]) : '#' }}">
                        <img src="{{ asset('storage/world/' . $id . '.png?ver=' . config('project.version')) }}" alt="{{ $world['label'] }}" class="world__preview">
                        <h3 class="world__title"><span>{{ $world['label'] }}</span></h3>
                        <div class="world__goto">
                            <span>{{ $world['empty'] ? "Coming Soon" : "Configure" }}</span>
                            @if (!$world['empty'])
                            <svg xmlns="http://www.w3.org/2000/svg" width="15.412" height="10.279" viewBox="0 0 15.412 10.279" class="svg--arrow-back">
                                <path d="M5.584.196a.7.7 0 01.005.985L2.34 4.441h12.378a.7.7 0 010 1.392H2.339l3.255 3.26a.7.7 0 01-.005.985.693.693 0 01-.98-.005L.198 5.63a.781.781 0 01-.145-.219.664.664 0 01-.054-.268.7.7 0 01.2-.487L4.61.213a.682.682 0 01.974-.017z" fill="#7992dc"></path>
                            </svg>
                            @endif
                        </div>
                    </a>
                </li>
                @endforeach
            </ul>
        </section>
    </main>

@endsection

