@extends('layouts.frontend', [
    'is_configurator' => true
])

@section('content')
    <div id="configurator">
        <configurator
            base-url="{{ route('frontend.configurator.index') }}"
            home="{{ route('frontend.configurator.index') }}"
            world='{{ $world }}'
            :products='@json($products, JSON_HEX_APOS)'
            :state='@json($state, JSON_HEX_APOS)'
            world_field_minimum_qty={{$world_field_minimum_qty}}
            api="{{ route('api.configurator.index') }}"
            message="{{ session('message', null) }}"
        ></configurator>
    </div>

@endsection

@push('footer')
<script>
new window.Vue({
    el: '#configurator'
});
</script>
@endpush
