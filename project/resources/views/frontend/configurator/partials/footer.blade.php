    <footer id="footer">
        <nav>
            <ul>
                <li><a href="/"><span class="has-svg"><svg class="svg--arrow-back" xmlns="http://www.w3.org/2000/svg"
                                width="15.412" height="10.279" viewBox="0 0 15.412 10.279">
                                <path
                                    d="M5.584.196a.7.7 0 01.005.985L2.34 4.441h12.378a.7.7 0 010 1.392H2.339l3.255 3.26a.7.7 0 01-.005.985.693.693 0 01-.98-.005L.198 5.63a.781.781 0 01-.145-.219.664.664 0 01-.054-.268.7.7 0 01.2-.487L4.61.213a.682.682 0 01.974-.017z"
                                    fill="#7992dc" />
                            </svg> </span><span>Home</span></a></li>
                <li><a href="#preview"><span class="has-svg"><svg class="svg--restart"
                                xmlns="http://www.w3.org/2000/svg" width="24.187" height="20.5"
                                viewBox="0 0 24.187 20.5">
                                <path
                                    d="M13.937.25a10 10 0 00-10 10H.604l4.322 4.322.078.156 4.489-4.478H6.16a7.815 7.815 0 112.289 5.489l-1.578 1.578A10 10 0 1013.937.25z"
                                    fill="#7a93dc" stroke="#fff" stroke-width=".5" />
                            </svg> </span><span>Ricomincia</span></a></li>
                <li><a href="#save"><span class="has-svg"><svg class="svg--confirm" xmlns="http://www.w3.org/2000/svg"
                                width="23.415" height="23.027" viewBox="0 0 23.415 23.027">
                                <g fill="none" stroke="#7a93dc" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="2">
                                    <path d="M22.002 10.553v.969a10.5 10.5 0 11-6.226-9.6" />
                                    <path d="M22.001 6.013l-10.499 10.51-3.15-3.15" />
                                </g>
                            </svg> </span><span>Salva</span></a></li>
                <li><a href="/stampa.html"><span class="has-svg"><svg class="svg--print"
                                xmlns="http://www.w3.org/2000/svg" width="24.771" height="24.772"
                                viewBox="0 0 24.771 24.772">
                                <g fill="none" stroke="#7a93dc" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="2">
                                    <path
                                        d="M5.554 8.97V1h13.663v7.97M5.554 19.217H3.277A2.277 2.277 0 011 16.94v-5.693A2.277 2.277 0 013.277 8.97h18.217a2.277 2.277 0 012.277 2.277v5.693a2.277 2.277 0 01-2.277 2.277h-2.277" />
                                    <path d="M5.554 14.663h13.663v9.109H5.554z" />
                                </g>
                            </svg> </span><span>Stampa</span></a></li>
            </ul>
        </nav>
    </footer>

    <script src="{{ asset('frontend/assets/js/app.js?ver=' . config('project.version')) }}"></script>
