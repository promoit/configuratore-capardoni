@extends('layouts.auth')

@section('content')
<div class="card bg-secondary border-0 mb-0">
    <div class="card-body px-lg-5 py-lg-5">

        <div class="text-center text-muted mb-4">
            <h3>Controla la tua casella di posta</h3>
        </div>

        @if (session('resent'))
            <div class="alert alert-success" role="alert">
                {{ __('A fresh verification link has been sent to your email address.') }}
            </div>
        @endif

        {{ __('Before proceeding, please check your email for a verification link.') }}
        {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.

    </div>
</div>
@endsection

