@extends('layouts.auth')

@section('title')
<div class="container">
    <div class="header-body text-center mb-4">
        <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 px-5">
                <h1 class="text-white">Ci sei quasi!</h1>
                <p class="text-lead text-white">Inserisci qui sotto la tua nuova password.</p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card bg-secondary border-0 mb-0">
    <div class="card-body px-lg-5 py-lg-5">

        <div class="text-center text-muted mb-4">
            <h3>Reimposta la password</h3>
        </div>
        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group mb-3">
                <div class="input-group input-group-merge input-group-alternative @error('email') is-invalid @enderror">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input id="email" type="email" class="form-control" name="email" placeholder="Indirizzo Email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                </div>

                @error('email')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror

            </div>
            <div class="form-group">
                <div class="input-group input-group-merge input-group-alternative @error('password') is-invalid @enderror">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input id="password" type="password" class="form-control" name="password" placeholder="Nuova password" value="" required autocomplete="new-password" autofocus>
                </div>

                @error('password')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror

            </div>

            <div class="form-group">
                <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input id="password-confirmation" type="password" class="form-control" name="password_confirmation" placeholder="Conferma password" value="" required autocomplete="new-password" autofocus>
                </div>
            </div>

            <div class="text-center">
                <button type="submit" class="btn btn-primary my-4">Reimposta la password</button>
            </div>
        </form>
    
    </div>
</div>
<div class="row mt-3">
    <div class="col text-center">
        <a href="{{ route('password.request') }}" class="text-light"><small>Recupero password</small></a>
    </div>
</div>
@endsection
