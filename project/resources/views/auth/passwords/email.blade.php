@extends('layouts.auth')

@section('title')
<div class="container">
    <div class="header-body text-center mb-4">
        <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 px-5">
                <h1 class="text-white">Eccoci qui!</h1>
                <p class="text-lead text-white">Inserisci qui sotto il tuo indirizzo email per recuperare la password.</p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card bg-secondary border-0 mb-0">
    <div class="card-body px-lg-5 py-lg-5">

        <div class="text-center text-muted mb-4">
            <h3>Reimposta la password</h3>
        </div>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="form-group mb-3">
                <div class="input-group input-group-merge input-group-alternative @error('email') is-invalid @enderror">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input id="email" type="email" class="form-control" name="email" placeholder="Indirizzo Email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                </div>

                @error('email')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror

            </div>

            <div class="text-center">
                <button type="submit" class="btn btn-primary my-4">Mandami il link di recupero</button>
            </div>
        </form>
    
    </div>
</div>
@endsection
