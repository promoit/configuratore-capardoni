<div class="col-lg-6">
    <div class="copyright text-center  text-lg-left  text-muted">
        &copy; {{ date('Y') }} <a href="https://www.opiquad.it/" class="font-weight-bold ml-1" target="_blank">Opiquad</a>
    </div>
</div>
<div class="col-lg-6">
    <ul class="nav nav-footer justify-content-center justify-content-lg-end">
        <li class="nav-item">
            <a href="https://www.opiquad.it/" class="nav-link" target="_blank">Opiquad</a>
        </li>
        <li class="nav-item">
            <a href="https://www.opiquad.it/chi-siamo/" class="nav-link" target="_blank">Chi siamo</a>
        </li>
        <li class="nav-item">
            <a href="https://www.opiquad.it/news/" class="nav-link" target="_blank">News</a>
        </li>
    </ul>
</div>
