<script src="{{ asset('backend/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('backend/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('backend/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('backend/vendor/js-cookie/js.cookie.js') }}"></script>
<script src="{{ asset('backend/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
<script src="{{ asset('backend/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') }}"></script>
<script src="{{ asset('backend/vendor/chart.js/dist/Chart.min.js') }}"></script>
<script src="{{ asset('backend/vendor/chart.js/dist/Chart.extension.js') }}"></script>
<script src="{{ asset('backend/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('backend/vendor/nouislider/distribute/nouislider.min.js') }}"></script>
<script src="{{ asset('backend/vendor/ace/ace.js') }}"></script>
<script src="{{ asset('backend/vendor/ace/ext-language_tools.js') }}"></script>
<script src="{{ asset('backend/vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('backend/vendor/upload/js/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('backend/vendor/upload/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('backend/vendor/upload/js/jquery.fileupload.js') }}"></script>
<script src="{{ asset('backend/vendor/selectize/js/selectize.min.js') }}"></script>
<script src="{{ asset('backend/vendor/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('backend/vendor/spectrum/spectrum.js') }}"></script>
<script src="{{ asset('backend/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('backend/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('backend/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('backend/vendor/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('backend/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('backend/vendor/datatables.net-select/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('backend/vendor/nouislider/distribute/nouislider.min.js') }}"></script>
<script src="{{ asset('backend/js/argon.js') }}"></script>
<script src="{{ asset('backend/js/backend.js?ver=' . config('project.version')) }}"></script>
<script src="{{ asset('backend/js/vue.js?ver=' . config('project.version')) }}"></script>

@stack('footer')

