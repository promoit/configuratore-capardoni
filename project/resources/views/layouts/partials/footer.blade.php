<footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
        @include('layouts.partials.credits')
    </div>
</footer>
