<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title ?? config('app.name') }}</title>
    <meta name="author" content="Filippo Toso - @@FilippoToso">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">

    <link href="{{ asset('backend/img/brand/favicon.png') }}" rel="icon" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="{{ asset('backend/vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/vendor/fontawesome/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/vendor/upload/css/jquery.fileupload.css') }}" type="text/css" rel="stylesheet"> 
    <link href="{{ asset('backend/vendor/jquery-ui/jquery-ui.min.css') }}" type="text/css" rel="stylesheet"> 
    <link href="{{ asset('backend/vendor/selectize/css/selectize.css') }}" type="text/css" rel="stylesheet"> 
    <link href="{{ asset('backend/vendor/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet"> 
    <link href="{{ asset('backend/vendor/sweetalert2/dist/sweetalert2.min.css') }}" type="text/css" rel="stylesheet"> 
	<link href="{{ asset('backend/vendor/spectrum/spectrum.css') }}" type="text/css" rel="stylesheet"> 
    <link href="{{ asset('backend/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" type="text/css" rel="stylesheet"> 
    <link href="{{ asset('backend/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}" type="text/css" rel="stylesheet"> 
    <link href="{{ asset('backend/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}" type="text/css" rel="stylesheet"> 
    <link href="{{ asset('backend/css/argon.css?ver=' . config('project.version')) }}" type="text/css" rel="stylesheet"> 
    <link href="{{ asset('backend/css/custom.css?ver=' . config('project.version')) }}" type="text/css" rel="stylesheet"> 
    @stack('head')
</head>
