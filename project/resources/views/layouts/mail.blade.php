<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- So that mobile will display zoomed in -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- enable media queries for windows phone 8 -->
    <meta name="format-detection" content="telephone=no">
    <!-- disable auto telephone linking in iOS -->
    <!--[if gte mso 9]><xml>
	 <o:OfficeDocumentSettings>
	  <o:AllowPNG/>
	  <o:PixelsPerInch>96</o:PixelsPerInch>
	 </o:OfficeDocumentSettings>
	</xml><![endif]-->
    <style>
        body {
            background-color: #ebeff7;
            margin: 0;
            padding: 10px;
        }
        
        .header,
        .title,
        .subtitle,
        .footer-text {
            font-family: Helvetica, Arial, sans-serif;
        }
        
        .container-padding {
            padding-left: 24px;
            padding-right: 24px;
        }
        
        .header {
            font-size: 16px;
            font-weight: normal;
            font-style: italic;
            text-transform: none;
            color: #ffffff;
            background: #ffffff;
            font-family: Verdana, sans-serif;
            text-align: left !important;
            padding: 0px 0px 0px 0px;
            line-height: 1.2;
            text-decoration: none;
        }
        
        .header a {
            font-size: 16px;
            font-weight: normal;
            font-style: italic;
            text-transform: none;
            color: #ffffff;
            font-family: Verdana, sans-serif;
            text-decoration: none;
        }
        
        .header-image {
            height: auto;
            vertical-align: top;
            border: none;
        }
        
        .full-width-header-image {
            width: 100%;
            display: block;
        }
        
        .footer-text,
        .footer-text th,
        .footer-text td,
        .footer-text li {
            font-size: 12px;
            line-height: 16px;
            color: #aaaaaa;
            background: #001f69;
        }
        
        .footer-text a,
        .footer-text th a,
        .footer-text td a,
        .footer-text li a {
            color: #aaaaaa;
        }
        
        .container {
            width: 600px;
            max-width: 600px;
        }
        
        .content {
            padding-top: 12px;
            padding-bottom: 12px;
            background-color: #ffffff;
        }
        
        code {
            background-color: #eee;
            padding: 0 4px;
            font-family: Courier, monospace;
            font-size: 12px;
        }
        
        hr {
            border: 0;
            border-bottom: 1px solid #cccccc;
        }
        
        .hr {
            height: 1px;
            border-bottom: 1px solid #cccccc;
        }
        
        h1 {
            font-size: 28px;
            font-weight: bold;
            font-style: normal;
            text-transform: none;
            color: #001f69;
            font-family: Verdana, sans-serif;
            text-align: left;
        }
        
        h2,
        h3,
        h4 {
            font-size: 18px;
            font-weight: bold;
            font-style: normal;
            text-transform: none;
            color: #001f69;
            font-family: Verdana, sans-serif;
            text-align: left;
        }
        
        .subtitle {
            font-size: 16px;
            font-weight: 600;
            color: #2469A0;
        }
        
        .subtitle span {
            font-weight: 400;
            color: #999999;
        }
        
        .body-text,
        .body-text td,
        .body-text th,
        .body-text li {
            font-family: Verdana, sans-serif;
            font-size: 16px;
            color: #001f69;
            font-weight: normal;
            font-style: normal;
            line-height: 1.5;
        }
        
        .body-text,
        .body-text td,
        .body-text th {
            text-align: left;
        }
        
        a {
            color: #7892dc;
            font-weight: bold;
            font-style: normal;
            text-transform: none;
            text-decoration: none;
        }
        
        .body-text .content-twocol td,
        .body-text .content-twocol h1,
        .body-text .content-twocol h2,
        .body-text .content-twocol h3,
        .body-text .content-twocol h4 {
            text-align: left;
            vertical-align: top;
        }
        
        body {
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        table {
            border-spacing: 0;
        }
        
        table td {
            border-collapse: collapse;
        }
        
        .ExternalClass {
            width: 100%;
        }
        
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }
        
        .ReadMsgBody {
            width: 100%;
            background-color: #ebeff7;
        }
        
        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
        
        img {
            -ms-interpolation-mode: bicubic;
        }
        
        .yshortcuts a {
            border-bottom: none !important;
        }
        /**** ADD CSS HERE ****/
        
        @media screen and (max-width: 599px) {
            table[class="force-row"],
            table[class="container"] {
                width: 100% !important;
                max-width: 100% !important;
            }
        }
        
        @media screen and (max-width: 400px) {
            td[class*="container-padding"] {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
            .header-image {
                max-width: 100% !important;
                height: auto !important;
            }
            table[class*="content-twocol"] {
                float: none;
            }
            table[class*="content-twocol"],
            table[class*="content-twocol"] p,
            table[class*="content-twocol"] h1,
            table[class*="content-twocol"] h2,
            table[class*="content-twocol"] h3,
            table[class*="content-twocol"] h4 {
                text-align: left !important;
            }
        }
        
        @media screen and (max-width: 400px) {}
        /**** ADD MOBILE CSS HERE ****/
        
        .ios-footer a {
            color: #aaaaaa !important;
            text-decoration: underline;
        }
    </style>
</head>

<body style="background-color: #ebeff7; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; margin: 0; padding: 0;">

    <span style="display:none; visibility:hidden; font-size: 1px !important;">{{ $abstract ?? null }}</span>

    <!-- 100% background wrapper (grey background) -->
    <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" class="###plugin-class###" style="border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
        <tr>
            <td align="center" valign="top" style="border-collapse: collapse;">

                <br>
                <table id="holder" class="container" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; max-width: 600px!important;" align="center">
                    <tr>
                        <td align="center" style="border-collapse: collapse;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                <tr>
                                    <td style="border-collapse: collapse;">

                                        <!--[if (gte mso 9)|(IE)]>
										<table id="outlookholder" class="container" width="600" border="0" cellspacing="0" cellpadding="0" align="center"><tr><td>
									<![endif]-->
                                        <!--header-table-->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                            <tr>
                                                <td class="container-padding header" align="left" style="border-collapse: collapse; padding-left: 24px; padding-right: 24px; background: #ffffff; color: #ffffff; font-family: Verdana, sans-serif; font-size: 16px; font-style: italic; font-weight: normal; line-height: 1.2; padding: 0px 0px 0px 0px; text-align: left !important; text-decoration: none; text-transform: none;">
                                                    <table width="100%" cellpadding="0" cellspacing="0" style="border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                                        <tr>
                                                            <td class="header-image" align="center" style="border-collapse: collapse; border: none; height: auto; vertical-align: top; text-align: center;">
                                                                <a href="{{ route('frontend.configurator.index') }}" style="color: #ffffff; font-family: Verdana, sans-serif; font-size: 16px; font-style: italic; font-weight: normal; text-decoration: none; text-transform: none;"><img class="header-image full-width-header-image" src="{{ asset('images/header-email.png') }}" style="-ms-interpolation-mode: bicubic; border: none; vertical-align: top; display: block; width: 600px; height: 118px;"
                                                                        width="600" height="118" alt="Capardoni Soluzioni per un packaging distintivo, innovativo e competitivo"></a>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </td>
                                            </tr>
                                        </table>
                                        <!--/header-table-->
                                        <!--content-table-->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                            <tr>
                                                <td class="content" align="" style="border-collapse: collapse; background-color: #ffffff; padding-bottom: 12px; padding-top: 12px;">
                                                    <div class="body-text" style="color: #001f69; font-family: Verdana, sans-serif; font-size: 16px; font-style: normal; font-weight: normal; line-height: 1.5; text-align: left;">
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="" style="border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                                            <tr>
                                                                <td class="container-padding" style="border-collapse: collapse; padding-left: 24px; padding-right: 24px; color: #001f69; font-family: Verdana, sans-serif; font-size: 16px; font-style: normal; font-weight: normal; line-height: 1.5; text-align: left;">
                                                                    @yield('content')
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--/content-table-->
                                        <!--footer-table-->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                            <tr>
                                                <td class="container-padding footer-text" align="left" style="border-collapse: collapse; font-family: Helvetica, Arial, sans-serif; padding-left: 24px; padding-right: 24px; background: #001f69; color: #aaaaaa; font-size: 12px; line-height: 16px;">
                                                    <p style="text-align: left;"><a style="font-style: normal; text-transform: none; color: #ffffff; font-family: 'Verdana', Arial, serif; font-size: 14px; text-decoration: none; font-weight: bold;" href="https://www.capardoni.com">Capardoni.com</a>                                                        <span style="color: #fff;
letter-spacing: 1px; font-size: 14px; font-weight: lighter;">&ndash; Beauty Full Pack</span>
                                                        <br>
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--/footer-table-->
                                        <!--[if (gte mso 9)|(IE)]>
										</td></tr></table>
									<![endif]-->

                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!--/100% background wrapper-->

</body>

</html>
