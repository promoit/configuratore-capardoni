<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        {!! file_get_contents(public_path('frontend/assets/css/pdf.css')) !!}
    </style>
</head>

<body @class([
    'has-double-header' => isset($has_double_header) && $has_double_header,
])>
    <header id="header" @class(['header'])>
        <table @class(['fixed-table']) width="100%" border="0" cellpadding="0">
            <tbody>
                <tr>
                    <td @class(['header__logo']) valign="middle">
                        <a href="{{ url('/') }}">
                            <img src="{{ public_path('images/pdf-logo.png') }}" width="656" height="120"
                                alt="Capardoni Soluzioni per un packaging distintivo, innovativo e competitivo">
                        </a>
                    </td>
                    <td @class(['header__quote']) align="right" valign="middle">
                        <p>
                            {{ __('pdf.header.right') }}
                            <a href="mailto:web@capardoni.com">web@capardoni.com</a>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
        @yield('header')
    </header>

    <footer id="footer" @class(['footer'])>
        <table @class(['fixed-table']) width="100%" border="0" cellpadding="0">
            <tbody>
                <tr>
                    <td @class(['footer__copy'])>
                        <p>{{ __('pdf.footer.copy', ['year' => date('Y')]) }}</p>
                    </td>
                </tr>
                <tr>
                    <td @class(['footer__disclaimer'])>
                        <p>{{ __('pdf.footer.disclaimer') }}</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </footer>

    <main id="content">
        @yield('content')
    </main>
</body>

</html>
