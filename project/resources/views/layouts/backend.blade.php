<!DOCTYPE html>
<html>
    @include('layouts.partials.head')
<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  d-flex  align-items-center">
        <a class="navbar-brand" href="{{ route('backend.dashboard') }}">
          <img src="{{ asset('backend/img/brand/blue.png') }}" class="navbar-brand-img" alt="...">
        </a>
        <div class=" ml-auto ">
          <!-- Sidenav toggler -->
          <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ $route == 'backend.dashboard'  ? 'active' : '' }}" href="{{ route('backend.dashboard') }}">
                        <i class="ni ni-tv-2 text-primary"></i> <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
            </ul>

            <hr class="my-2">

            @role(\App\Models\Role::ADMIN)
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ $route == 'backend.admin.users'  ? 'active' : '' }}" href="{{ route('backend.admin.users.index') }}">
                        <i class="fa fa-user text-info"></i> <span class="nav-link-text">Utenti</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ $route == 'backend.admin.products'  ? 'active' : '' }}" href="{{ route('backend.admin.products.edit') }}">
                        <i class="fa fa-box text-primary"></i> <span class="nav-link-text">Prodotti</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ $route == 'backend.admin.configurator'  ? 'active' : '' }}" href="{{ route('backend.admin.configurator.edit') }}">
                        <i class="fa fa-cog text-success"></i> <span class="nav-link-text">Configurazione</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ str_contains(url()->current(), 'quote')  ? 'active' : '' }}" href="{{ route('backend.admin.leads','quote') }}">
                        <i class="fa fa-envelope" aria-hidden="true"></i> <span class="nav-link-text">Leads Quote</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ str_contains(url()->current(), 'save')  ? 'active' : '' }}" href="{{ route('backend.admin.leads', 'save') }}">
                        <i class="fa fa-envelope" aria-hidden="true"></i> <span class="nav-link-text">Leads Save</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ str_contains(url()->current(), 'download')  ? 'active' : '' }}" href="{{ route('backend.admin.leads', 'download') }}">
                        <i class="fa fa-envelope" aria-hidden="true"></i> <span class="nav-link-text">Leads Download</span>
                    </a>
                </li>
            </ul>

            <hr class="my-2">
            @endrole

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="mailto:{{ config('project.support.email') }}">
                        <i class="ni ni-support-16 text-warning"></i> <span class="nav-link-text">Supporto</span>
                    </a>
                </li>
            </ul>

        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Navbar links -->
          <ul class="navbar-nav align-items-center  ml-md-auto ">
            <li class="nav-item d-xl-none">
              <!-- Sidenav toggler -->
              <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </div>
            </li>
          </ul>

        <!-- User -->
          <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
            <li class="nav-item dropdown">
              <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle">
                        <img alt="{{ request()->user()->name ?? '' }}" src="{{ Gravatar::get(request()->user()->email ?? '', 200, 'mp') }}">
                  </span>
                  <div class="media-body  ml-2  d-none d-lg-block">
                    <span class="mb-0 text-sm  font-weight-bold">{{ request()->user()->name ?? '' }}</span>
                  </div>
                </div>
              </a>
              <div class="dropdown-menu  dropdown-menu-right ">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0">Ciao!</h6>
                </div>

                <a href="{{ route('backend.me.edit') }}" class="dropdown-item">
                    <i class="ni ni-single-02"></i>
                    <span>Profilo</span>
                </a>

                @if(request()->user()->isImpersonating())
                <a href="{{ route('backend.me.depersonate') }}" class="dropdown-item">
                    <i class="ni ni-curved-next"></i>
                    <span>Cambia utente</span>
                </a>
                @endif
                <div class="dropdown-divider"></div>
                <a href="{{ route('logout') }}" class="dropdown-item">
                    <i class="ni ni-user-run"></i>
                    <span>Logout</span>
                </a>

            </div>
            </li>
          </ul>

        </div>
      </div>
    </nav>

    <div class="header bg-primary pb-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                    <div class="col">
                        @include('laravel-helpers::breadcrumbs')
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        @include('flash::message')
                    </div>
                </div>
                @stack('header')
            </div>
        </div>
    </div>

    <div class="container-fluid mt--8">
        @yield('content')

        @include('layouts.partials.footer')
    </div>
  </div>

    @include('layouts.partials.scripts')
</body>
</html>
