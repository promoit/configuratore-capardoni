<!DOCTYPE html>
<html>
@include('layouts.partials.head')

<body class="bg-default">

    <div class="main-content">
        <div class="header bg-gradient-primary pt-6 pb-8 pb-lg-8 pt-lg-6">
            @yield('title')
            <div class="separator separator-bottom separator-skew zindex-100">
                <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
                </svg>
            </div>
        </div>
        <!-- Page content -->
        <div class="container mt--8 pb-5">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="py-5" id="footer-main">
        <div class="container">
            <div class="row align-items-center justify-content-xl-between">
                @include('layouts.partials.credits')
            </div>
        </div>
    </footer>

</body>
</html>