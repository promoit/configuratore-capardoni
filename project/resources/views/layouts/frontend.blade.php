<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $title ?? config('app.name') }}</title>
    <meta property="og:locale" content="it_IT" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Capardoni" />
    <meta property="og:site_name" content="Capardoni" />

    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="application-name" content="Capardoni" />
    <meta name="apple-mobile-web-app-title" content="Capardoni" />
    <meta name="theme-color" content="#001e5f" />
    <meta name="msapplication-navbutton-color" content="#001e5f" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="msapplication-starturl" content="{{ route($view . '.index') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <link rel="shortcut icon" href="{{ asset('frontend/assets/icons/ios/16.png?ver=' . config('project.version')) }}" />
    <meta name="msapplication-TileImage" content="{{ asset('frontend/assetsicons/windows11/Square150x150Logo.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="71x71" href="{{ asset('frontend/assets/icons/windows11/SmallTile.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="71x71" href="{{ asset('frontend/assets/icons/windows11/SmallTile.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="89x89" href="{{ asset('frontend/assets/icons/windows11/SmallTile.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="89x89" href="{{ asset('frontend/assets/icons/windows11/SmallTile.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="107x107" href="{{ asset('frontend/assets/icons/windows11/SmallTile.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="107x107" href="{{ asset('frontend/assets/icons/windows11/SmallTile.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="142x142" href="{{ asset('frontend/assets/icons/windows11/SmallTile.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="142x142" href="{{ asset('frontend/assets/icons/windows11/SmallTile.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="284x284" href="{{ asset('frontend/assets/icons/windows11/SmallTile.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="284x284" href="{{ asset('frontend/assets/icons/windows11/SmallTile.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="150x150" href="{{ asset('frontend/assets/icons/windows11/Square150x150Logo.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="150x150" href="{{ asset('frontend/assets/icons/windows11/Square150x150Logo.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="188x188" href="{{ asset('frontend/assets/icons/windows11/Square150x150Logo.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="188x188" href="{{ asset('frontend/assets/icons/windows11/Square150x150Logo.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="225x225" href="{{ asset('frontend/assets/icons/windows11/Square150x150Logo.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="225x225" href="{{ asset('frontend/assets/icons/windows11/Square150x150Logo.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="300x300" href="{{ asset('frontend/assets/icons/windows11/Square150x150Logo.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="300x300" href="{{ asset('frontend/assets/icons/windows11/Square150x150Logo.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="600x600" href="{{ asset('frontend/assets/icons/windows11/Square150x150Logo.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="600x600" href="{{ asset('frontend/assets/icons/windows11/Square150x150Logo.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="310x150" href="{{ asset('frontend/assets/icons/windows11/Wide310x150Logo.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="310x150" href="{{ asset('frontend/assets/icons/windows11/Wide310x150Logo.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="388x188" href="{{ asset('frontend/assets/icons/windows11/Wide310x150Logo.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="388x188" href="{{ asset('frontend/assets/icons/windows11/Wide310x150Logo.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="465x225" href="{{ asset('frontend/assets/icons/windows11/Wide310x150Logo.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="465x225" href="{{ asset('frontend/assets/icons/windows11/Wide310x150Logo.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="620x300" href="{{ asset('frontend/assets/icons/windows11/Wide310x150Logo.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="620x300" href="{{ asset('frontend/assets/icons/windows11/Wide310x150Logo.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="1240x600" href="{{ asset('frontend/assets/icons/windows11/Wide310x150Logo.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="1240x600" href="{{ asset('frontend/assets/icons/windows11/Wide310x150Logo.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="310x310" href="{{ asset('frontend/assets/icons/windows11/LargeTile.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="310x310" href="{{ asset('frontend/assets/icons/windows11/LargeTile.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="388x388" href="{{ asset('frontend/assets/icons/windows11/LargeTile.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="388x388" href="{{ asset('frontend/assets/icons/windows11/LargeTile.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="465x465" href="{{ asset('frontend/assets/icons/windows11/LargeTile.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="465x465" href="{{ asset('frontend/assets/icons/windows11/LargeTile.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="620x620" href="{{ asset('frontend/assets/icons/windows11/LargeTile.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="620x620" href="{{ asset('frontend/assets/icons/windows11/LargeTile.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="1240x1240" href="{{ asset('frontend/assets/icons/windows11/LargeTile.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="1240x1240" href="{{ asset('frontend/assets/icons/windows11/LargeTile.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="44x44" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="44x44" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="55x55" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="55x55" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="66x66" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="66x66" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="88x88" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="88x88" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="176x176" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="176x176" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="50x50" href="{{ asset('frontend/assets/icons/windows11/StoreLogo.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="50x50" href="{{ asset('frontend/assets/icons/windows11/StoreLogo.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="63x63" href="{{ asset('frontend/assets/icons/windows11/StoreLogo.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="63x63" href="{{ asset('frontend/assets/icons/windows11/StoreLogo.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="75x75" href="{{ asset('frontend/assets/icons/windows11/StoreLogo.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="75x75" href="{{ asset('frontend/assets/icons/windows11/StoreLogo.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="100x100" href="{{ asset('frontend/assets/icons/windows11/StoreLogo.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="100x100" href="{{ asset('frontend/assets/icons/windows11/StoreLogo.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="200x200" href="{{ asset('frontend/assets/icons/windows11/StoreLogo.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="200x200" href="{{ asset('frontend/assets/icons/windows11/StoreLogo.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="620x300" href="{{ asset('frontend/assets/icons/windows11/SplashScreen.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="620x300" href="{{ asset('frontend/assets/icons/windows11/SplashScreen.scale-100.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="775x375" href="{{ asset('frontend/assets/icons/windows11/SplashScreen.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="775x375" href="{{ asset('frontend/assets/icons/windows11/SplashScreen.scale-125.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="930x450" href="{{ asset('frontend/assets/icons/windows11/SplashScreen.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="930x450" href="{{ asset('frontend/assets/icons/windows11/SplashScreen.scale-150.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="1240x600" href="{{ asset('frontend/assets/icons/windows11/SplashScreen.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="1240x600" href="{{ asset('frontend/assets/icons/windows11/SplashScreen.scale-200.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="2480x1200" href="{{ asset('frontend/assets/icons/windows11/SplashScreen.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="2480x1200" href="{{ asset('frontend/assets/icons/windows11/SplashScreen.scale-400.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="16x16" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-16.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="16x16" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-16.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="20x20" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-20.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="20x20" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-20.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="24x24" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-24.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="24x24" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-24.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="30x30" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-30.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="30x30" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-30.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="32x32" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-32.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="32x32" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-32.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="36x36" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-36.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="36x36" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-36.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="40x40" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-40.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="40x40" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-40.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="44x44" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-44.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="44x44" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-44.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="48x48" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-48.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="48x48" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-48.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="60x60" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-60.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-60.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="64x64" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-64.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="64x64" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-64.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="72x72" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-72.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-72.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="80x80" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-80.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="80x80" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-80.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="96x96" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-96.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="96x96" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-96.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="256x256" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-256.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="256x256" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.targetsize-256.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="16x16" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-16.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="16x16" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-16.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="20x20" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-20.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="20x20" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-20.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="24x24" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-24.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="24x24" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-24.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="30x30" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-30.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="30x30" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-30.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="32x32" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-32.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="32x32" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-32.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="36x36" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-36.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="36x36" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-36.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="40x40" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-40.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="40x40" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-40.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="44x44" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-44.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="44x44" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-44.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="48x48" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-48.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="48x48" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-48.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="60x60" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-60.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-60.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="64x64" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-64.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="64x64" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-64.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="72x72" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-72.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-72.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="80x80" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-80.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="80x80" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-80.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="96x96" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-96.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="96x96" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-96.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="256x256" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-256.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="256x256" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-unplated_targetsize-256.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="16x16" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-16.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="16x16" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-16.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="20x20" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-20.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="20x20" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-20.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="24x24" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-24.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="24x24" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-24.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="30x30" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-30.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="30x30" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-30.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="32x32" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-32.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="32x32" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-32.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="36x36" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-36.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="36x36" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-36.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="40x40" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-40.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="40x40" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-40.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="44x44" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-44.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="44x44" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-44.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="48x48" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-48.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="48x48" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-48.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="60x60" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-60.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-60.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="64x64" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-64.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="64x64" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-64.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="72x72" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-72.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-72.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="80x80" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-80.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="80x80" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-80.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="96x96" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-96.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="96x96" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-96.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="256x256" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-256.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="256x256" href="{{ asset('frontend/assets/icons/windows11/Square44x44Logo.altform-lightunplated_targetsize-256.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="512x512" href="{{ asset('frontend/assets/icons/android/android-launchericon-512-512.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="512x512" href="{{ asset('frontend/assets/icons/android/android-launchericon-512-512.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="192x192" href="{{ asset('frontend/assets/icons/android/android-launchericon-192-192.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="192x192" href="{{ asset('frontend/assets/icons/android/android-launchericon-192-192.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="144x144" href="{{ asset('frontend/assets/icons/android/android-launchericon-144-144.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('frontend/assets/icons/android/android-launchericon-144-144.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="96x96" href="{{ asset('frontend/assets/icons/android/android-launchericon-96-96.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="96x96" href="{{ asset('frontend/assets/icons/android/android-launchericon-96-96.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="72x72" href="{{ asset('frontend/assets/icons/android/android-launchericon-72-72.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('frontend/assets/icons/android/android-launchericon-72-72.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="48x48" href="{{ asset('frontend/assets/icons/android/android-launchericon-48-48.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="48x48" href="{{ asset('frontend/assets/icons/android/android-launchericon-48-48.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="16x16" href="{{ asset('frontend/assets/icons/ios/16.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="16x16" href="{{ asset('frontend/assets/icons/ios/16.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="20x20" href="{{ asset('frontend/assets/icons/ios/20.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="20x20" href="{{ asset('frontend/assets/icons/ios/20.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="29x29" href="{{ asset('frontend/assets/icons/ios/29.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="29x29" href="{{ asset('frontend/assets/icons/ios/29.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="32x32" href="{{ asset('frontend/assets/icons/ios/32.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="32x32" href="{{ asset('frontend/assets/icons/ios/32.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="40x40" href="{{ asset('frontend/assets/icons/ios/40.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="40x40" href="{{ asset('frontend/assets/icons/ios/40.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="50x50" href="{{ asset('frontend/assets/icons/ios/50.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="50x50" href="{{ asset('frontend/assets/icons/ios/50.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="57x57" href="{{ asset('frontend/assets/icons/ios/57.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('frontend/assets/icons/ios/57.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="58x58" href="{{ asset('frontend/assets/icons/ios/58.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="58x58" href="{{ asset('frontend/assets/icons/ios/58.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="60x60" href="{{ asset('frontend/assets/icons/ios/60.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('frontend/assets/icons/ios/60.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="64x64" href="{{ asset('frontend/assets/icons/ios/64.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="64x64" href="{{ asset('frontend/assets/icons/ios/64.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="72x72" href="{{ asset('frontend/assets/icons/ios/72.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('frontend/assets/icons/ios/72.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="76x76" href="{{ asset('frontend/assets/icons/ios/76.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('frontend/assets/icons/ios/76.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="80x80" href="{{ asset('frontend/assets/icons/ios/80.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="80x80" href="{{ asset('frontend/assets/icons/ios/80.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="87x87" href="{{ asset('frontend/assets/icons/ios/87.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="87x87" href="{{ asset('frontend/assets/icons/ios/87.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="100x100" href="{{ asset('frontend/assets/icons/ios/100.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="100x100" href="{{ asset('frontend/assets/icons/ios/100.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="114x114" href="{{ asset('frontend/assets/icons/ios/114.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('frontend/assets/icons/ios/114.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="120x120" href="{{ asset('frontend/assets/icons/ios/120.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('frontend/assets/icons/ios/120.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="128x128" href="{{ asset('frontend/assets/icons/ios/128.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="128x128" href="{{ asset('frontend/assets/icons/ios/128.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="144x144" href="{{ asset('frontend/assets/icons/ios/144.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('frontend/assets/icons/ios/144.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="152x152" href="{{ asset('frontend/assets/icons/ios/152.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('frontend/assets/icons/ios/152.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="167x167" href="{{ asset('frontend/assets/icons/ios/167.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="167x167" href="{{ asset('frontend/assets/icons/ios/167.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="180x180" href="{{ asset('frontend/assets/icons/ios/180.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('frontend/assets/icons/ios/180.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="192x192" href="{{ asset('frontend/assets/icons/ios/192.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="192x192" href="{{ asset('frontend/assets/icons/ios/192.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="256x256" href="{{ asset('frontend/assets/icons/ios/256.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="256x256" href="{{ asset('frontend/assets/icons/ios/256.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="512x512" href="{{ asset('frontend/assets/icons/ios/512.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="512x512" href="{{ asset('frontend/assets/icons/ios/512.png?ver=' . config('project.version')) }}" />
    <link rel="icon" sizes="1024x1024" href="{{ asset('frontend/assets/icons/ios/1024.png?ver=' . config('project.version')) }}" />
    <link rel="apple-touch-icon" sizes="1024x1024" href="{{ asset('frontend/assets/icons/ios/1024.png?ver=' . config('project.version')) }}" />

    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-regular-webfont.woff2') }}" as="font" type="font/woff2" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-regular-webfont.eot') }}" as="font" type="font/eot" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-regular-webfont.ttf') }}" as="font" type="font/ttf" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-light-webfont.woff2') }}" as="font" type="font/woff2" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-light-webfont.eot') }}" as="font" type="font/eot" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-light-webfont.ttf') }}" as="font" type="font/ttf" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-medium-webfont.woff2') }}" as="font" type="font/woff2" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-medium-webfont.eot') }}" as="font" type="font/eot" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-medium-webfont.ttf') }}" as="font" type="font/ttf" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-semibold-webfont.woff2') }}" as="font" type="font/woff2" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-semibold-webfont.eot') }}" as="font" type="font/eot" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-semibold-webfont.ttf') }}" as="font" type="font/ttf" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-bold-webfont.woff2') }}" as="font" type="font/woff2" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-bold-webfont.eot') }}" as="font" type="font/eot" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-bold-webfont.ttf') }}" as="font" type="font/ttf" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-extralight-webfont.woff2') }}" as="font" type="font/woff2" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-extralight-webfont.eot') }}" as="font" type="font/eot" crossOrigin="anonymous" />
    <link rel="preload" href="{{ asset('frontend/assets/fonts/wotfard-extralight-webfont.ttf') }}" as="font" type="font/ttf" crossOrigin="anonymous" />
    @if (config('project.google.tag_manager') && config('app.env') === 'production')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','{{ config('project.google.tag_manager') }}');</script>
    <!-- End Google Tag Manager -->
    @endif

    <link href="{{ asset('frontend/assets/css/main.css?ver=' . config('project.asset_version')) }}" rel="stylesheet">

    @stack('head')
</head>

<body class="{{ isset($is_configurator) && $is_configurator ? 'has-products' : '' }}">
    @if (config('project.google.tag_manager') && config('app.env') === 'production')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ config('project.google.tag_manager') }}"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @endif

    @yield('content')

    <script src="{{ asset('frontend/assets/js/app.js?ver=' . config('project.asset_version')) }}"></script>
    @if (isset($is_configurator) && $is_configurator)
    <script src="{{ asset('frontend/js/vue.js?ver=' . config('project.asset_version')) }}"></script>
    @endif

    @stack('footer')
</body>

</html>
