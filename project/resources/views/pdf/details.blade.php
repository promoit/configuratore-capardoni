@extends('layouts.pdf', [
    'has_double_header' => true,
])

@section('header')
<table width="100%" border="0" cellpadding="0">
    <tbody>
        <tr>
            <td @class(['header__title']) valign="middle">
                    @if ($combination)
                        <h1>{{ $combination->title }}</h1>
                    @endif
                    </td>
                    @if ($world)
                        <td @class(['header__breadcrumb']) align="right" valign="middle">
                            <ul>
                                <li @class(['pdf-place-category'])>&nbsp;</li>
                                <li @class(['pdf-place-world'])>&nbsp;</li>
                            </ul>
                        </td>
                    @endif
                </tr>
            </tbody>
        </table>
@endsection

@section('content')
    @if ($combination)
        <table @class(['combination', 'table']) width="100%" border="0" cellpadding="0">
            <tbody>
                <tr>
                    <td @class(['combination__thumbnail', 'table__left']) valign="top" align="right" width="56mm">
                        <img src="{{ $combination->image_path }}" alt="{{ $combination->title }}" width="620"
                            height="620" />
                    </td>
                    <td @class(['combination__content', 'table__right']) valign="top" align="left">
                        <h2>{{ __('pdf.content.configuration') }}</h2>

                        <table @class(['combination__products']) width="100%" border="0" cellpadding="0">
                            @foreach ($products as $product)
                                <tr>
                                    <td valign="middle" align="left">
                                        <strong>{{ $product->type }}</strong>
                                    </td>
                                    <td valign="middle" align="left">
                                        <span>{{ $product->name }}</span>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    @endif

    @if ($products && !empty($products))
        @foreach ($products as $product)
            <table @class(['product', 'table']) width="100%" border="0" cellpadding="0">
                <thead>
                    <tr>
                        <th @class(['product__type', 'table__left']) valign="middle" align="left" width="56mm">
                            <span>{{ $product->type }}</span>
                        </th>
                        <th @class(['product__title', 'table__right']) valign="middle" align="left">
                            <h3>{{ $product->name }}</h3>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td @class(['product__thumbnail', 'table__left']) valign="top" align="left" width="56mm">
                            <img src="{{ $product->image_path }}" alt="{{ $product->title }}" width="400"
                                height="400" />
                        </td>
                        <td @class(['product__content', 'table__right']) valign="top" align="left">
                            <table @class(['product__attributes']) width="100%" border="0" cellpadding="0">
                                @foreach ($product->all_attributes as $attribute)
                                    @if ($attribute['value'])
                                        <tr>
                                            <td valign="middle" align="left" width="auto">
                                                <strong>{{ $attribute['name'] }}</strong>
                                            </td>
                                            <td valign="middle" align="left">
                                                <span>{{ $attribute['value'] }}</span>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        @endforeach
    @endif
@endsection
