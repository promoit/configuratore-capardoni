@set('recaptchaSiteKey', optional($project->configuration)->recaptcha_key ?? config('services.recaptcha.key'))
<div class="g-recaptcha" data-sitekey="{{ $recaptchaSiteKey }}" data-size="invisible" data-callback="setRecaptchaResponse"></div>
<input type="hidden" id="recaptcha" name="recaptcha" />
<script>
var onRecaptchaLoaded = function() {
    grecaptcha.execute();
};

function setRecaptchaResponse(response) { 
    document.getElementById('recaptcha').value = response; 
}
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onRecaptchaLoaded" async defer></script>
