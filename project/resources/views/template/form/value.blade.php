<div class="form-group focused">
    <label class="form-control-label">{{ $field->label }}</label>
    <span class="span-control form-control-alternative">{{ $field->value }}</span>
</div>
