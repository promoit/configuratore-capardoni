<div class="row mt-3">
    <label class="col-sm-2 col-form-label label-checkbox">{{ $field->title }}</label>
    <div class="col-sm-10 checkbox-radios">
        <div class="form-check">
            
            @if($error = $field->error())
                <label for="{{ $field->id() }}" class="bmd-label-floating">{{ $error }}</label>
            @endif

            <label class="form-check-label">
                <input type="checkbox" class="form-check-input {{ $field->class }}" id="{{ $field->id() }}" name="{{ $field->name }}" value="1" {{ $field->required() }} {{ $field->readonly() }}  {{ $field->disabled() }} {{ $field->checked() }}> {{ $field->label }}
                <span class="form-check-sign">
                    <span class="check"></span>
                </span>
            </label>

            @if($error)
                <span class="form-control-feedback">
                    <i class="material-icons">clear</i>
                </span>
            @endif    
        </div>
    </div>
</div>
