<div class="row">
    <label class="col-sm-2 col-form-label">{{ $field->label }}</label>
    <div class="col-sm-10">
        <div class="form-group has-info">

        @if($error = $field->error())
            <label for="{{ $field->id() }}" class="bmd-label-floating">{{ $error }}</label>
        @endif

            <input type="password" class="form-control {{ $field->class }}" id="{{ $field->id() }}" name="{{ $field->name }}" value="{{ $field->old() }}" {{ $field->required() }} {{ $field->readonly() }}  {{ $field->disabled() }}>

        @if($error)
            <span class="form-control-feedback">
                <i class="material-icons">clear</i>
            </span>
        @endif        

        </div>
    </div>
</div>
