<div class="row">
    <label class="col-sm-2 col-form-label label-checkbox">{{ $field->title }}</label>
    <div class="col-sm-10 checkbox-radios">
        @if($error = $field->error())
            <label for="{{ $field->id() }}" class="bmd-label-floating">{{ $error }}</label>
        @endif

        @foreach ($field->values as $key => $value)

            <div class="form-check">
                <label class="form-check-label">
                    <input type="checkbox" class="form-check-input {{ $field->class }}" id="{{ $field->id($key) }}" name="{{ $field->name }}" value="{{ $field->old($value) }}" {{ $field->required() }} {{ $field->readonly() }}  {{ $field->disabled() }} {{ $field->checked($value) }}> {{ $field->label }}
                    <span class="form-check-sign">
                        <span class="check"></span>
                    </span>
                </label>
            </div>

        @endforeach

        @if($error)
            <span class="form-control-feedback">
                <i class="material-icons">clear</i>
            </span>
        @endif    

    </div>
</div>