@set('error', $field->error())
<div class="form-group">
    <div class="custom-control custom-checkbox mb-3">
        <input type="checkbox" id="{{ $field->id() }}" class="custom-control-input {{ $field->class }} {{ ($error) ? 'is-invalid' : '' }}" name="{{ $field->name }}" placeholder="{{ ($error) ? $error : $field->placeholder }}" value="{{ is_bool($field->value) ? 1 : $field->old() }}" {{ $field->required() }} {{ $field->readonly() }}  {{ $field->disabled() }} {{ $field->checked() }}> 
        <label for="{{ $field->id() }}" class="custom-control-label">{{ $field->label }}</label>
    </div>
    @if ($error)
    <div class="invalid-feedback">{{ $error }}</div>
    @endif
</div>
@unset($error)

