@set('error', $field->error())
<div class="form-group">
    @include('template.form.label')
    <textarea class="form-control {{ $field->class }} {{ ($error) ? 'is-invalid' : '' }}" id="{{ $field->id() }}" name="{{ $field->name }}" {{ $field->required() }} {{ $field->readonly() }}  {{ $field->disabled() }} rows="{{ $field->rows }}">{{ $field->old() }}</textarea>
    @if ($error)
    <div class="invalid-feedback">{{ $error }}</div>
    @endif
</div>
@unset($error)