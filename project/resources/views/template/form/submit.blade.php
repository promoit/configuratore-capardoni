@if ($field->icon)
    <button class="btn btn-icon btn-3 {{ $field->class }}" type="submit">
        <span class="btn-inner--icon"><i class="{{ $field->icon }}"></i></span>
        <span class="btn-inner--text">{{ $field->label }}</span>
    </button>
@else
    <button class="btn {{ $field->class }}" type="submit">{{ $field->label }}</button>
@endif
