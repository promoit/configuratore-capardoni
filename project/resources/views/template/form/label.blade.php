@if ($field->label)
    <label class="form-control-label" for="{{ $field->id() }}">{{ $field->label }} {!! $field->required ? '<span>*</span>' : '' !!}</label>
@endif
