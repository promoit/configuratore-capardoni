@set('error', $field->error())
<div class="form-group">
    @include('template.form.label')
    <input type="password" id="{{ $field->id() }}" class="form-control form-control-alternative {{ $field->class }} {{ ($error) ? 'is-invalid' : '' }}" name="{{ $field->name }}" placeholder="{{ ($error) ? $error : $field->placeholder }}" value="{{ $field->old() }}" {{ $field->required() }} {{ $field->readonly() }}  {{ $field->disabled() }}>
    @if ($error)
    <div class="invalid-feedback">{{ $error }}</div>
    @endif
</div>
@unset($error)