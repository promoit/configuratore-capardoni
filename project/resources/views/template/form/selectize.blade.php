@set('error', $field->error())
<div>
    @if ($field->label)
    @include('template.form.label')
    @endif
    <select class="{{ $field->class }} {{ ($error) ? 'is-invalid' : '' }}" name="{{ $field->name }}" id="{{ $field->id() }}" {{ $field->required() }} {{ $field->readonly() }}  {{ $field->disabled() }} {{ $field->multiple() }} {!! $field->attributes !!}>
        <option value="">Scegli un valore</option>
        @foreach($field->values as $key => $value)
            <option value="{{ $key }}" {{ $field->selected($key) }}>{{ $value }}</option>
        @endforeach
    </select>
    
    @if ($error)
    <div class="invalid-feedback">{{ $error }}</div>
    @endif
</div>
@unset($error)