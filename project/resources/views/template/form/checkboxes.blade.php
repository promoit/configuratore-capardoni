@set('error', $field->error())
<div class="form-group">
    <p>{{ $field->title }}</p>
    <div class="row">

        @foreach ($field->values as $key => $value)
        <div class="{{ $field->class ?? 'col' }}">
            <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" id="{{ $field->id($key) }}" class="custom-control-input {{ $field->class }} {{ ($error) ? 'is-invalid' : '' }}" name="{{ $field->name }}" value="{{ $key }}" {{ $field->required() }} {{ $field->readonly() }}  {{ $field->disabled() }} {{ in_array($key, old($field->name(), $field->value)) ? 'checked' : '' }}> 
                <label for="{{ $field->id($key) }}" class="custom-control-label">{{ $value }}</label>
            </div>
        </div>  
        @endforeach

    </div>

    @if ($error)
        <div class="invalid-feedback">{{ $error }}</div>
    @endif
</div>
@unset($error)
