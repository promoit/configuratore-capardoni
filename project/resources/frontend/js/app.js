import 'normalize.css';

require('fslightbox');

if (module.hot) {
    module.hot.accept();
}

function getElementFromFragment(target) {
    if (!target.href || target.href.indexOf('#') === -1) {
        return false;
    }

    const id = getFragmentFromUrl(target.href);
    if (!id) {
        return false;
    }

    const element = document.getElementById(id);
    if (!element) {
        return false;
    }

    return element;
}

function getFragmentFromUrl(url) {
    const hrefParts = url.split('#');

    if (hrefParts.length === 0) {
        return false;
    }

    const fragment = hrefParts[hrefParts.length - 1];

    return fragment;
}

function isDesktop() {
    return window.innerWidth >= 768;
}

function hasScrollbar(element) {
    return {
        x: element.scrollWidth > element.clientWidth,
        y: element.scrollHeight > element.clientHeight
    };
}

function isScrolledIntoView(element, partial, offset) {
    const rect = element.getBoundingClientRect();
    const top = rect.top + (offset.top || 0);
    const bottom = rect.bottom - (offset.bottom || 0);

    let isVisible = false;

    if (partial) {
        // Partially visible elements return true:
        isVisible = top < window.innerHeight && bottom >= 0;
    } else {
        isVisible = (top >= 0) && (bottom <= window.innerHeight);
    }

    return isVisible;
}

window.addEventListener('load', () => {
    /*
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker
            .register('service-worker.js')
            .then((registration) => {
                console.log('Registered!');
            }, (err) => {
                console.log('ServiceWorker registration failed: ', err);
            })
            .catch((err) => {
                console.log(err);
            });
    }
    // */

    const header = document.getElementById('header');
    // const anchors = document.getElementById('anchors');
    const main = document.getElementById('main');
    const footer = document.getElementById('footer');

    const preview = main.querySelector('#preview');
    /*
    const previewThumbnail = preview.querySelector('#thumbnail');

    const links = document.getElementsByTagName('a');
    const tabsHolders = document.getElementsByClassName('js-tabs');
    const accordions = document.getElementsByClassName('js-accordion');
    const categories = document.getElementsByClassName('js-category');
    const filtersTriggers = document.getElementsByClassName('js-filters-trigger');
    const popupOpeners = document.getElementsByClassName('js-popup-opener');
    // */
    const setMainMinHeight = function() {
        const diff = footer.offsetHeight + header.offsetHeight;

        if (isDesktop()) {
            main.style.marginTop = `${header.offsetHeight}px`;
        } else {
            main.style.marginTop = null;
        }

        main.style.marginBottom = `${footer.offsetHeight}px`;
        main.style.minHeight = `${window.innerHeight - diff}px`;
    };

    const setStickyTop = function() {
        let top = null;
        if (isDesktop()) {
            const style = getComputedStyle(main);
            const offset = parseFloat(style.getPropertyValue('border-top-width'));
            top = `${header.offsetHeight + offset}px`;
        }

        if (preview) {
            preview.style.top = top;
        }

    };

    /*
    const changeThumbnail = function(src) {
        if (!src) {
            return;
        }

        const image = previewThumbnail.querySelector('img');

        if (!image) {
            return;
        }

        image.src = src;
    };
    // */

    setStickyTop();
    setMainMinHeight();

    window.addEventListener('resize', () => {
        setStickyTop();
        setMainMinHeight();
    });

    /*
    window.addEventListener('scroll', () => {
        if (!isDesktop()) {
            if (categories.length > 0) {
                for (const category of categories) {
                    if (isScrolledIntoView(category, true, { bottom: anchors.offsetHeight })) {
                        const active = anchors.querySelector('.is-active');
                        const anchor = anchors.querySelector(`[href="#${category.id}"]`);

                        if (active !== anchor) {
                            if (active) {
                                active.classList.remove('is-active');
                            }

                            anchor.classList.add('is-active');
                            anchors.querySelector('ul').scroll({
                                left: (anchor.offsetLeft - 15) || 0,
                                behavior: 'smooth'
                            });
                        }
                        break;
                    }
                }
            }
        }
    });

    if (accordions.length > 0) {
        Array.from(accordions).forEach((accordion) => {
            const parts = accordion.querySelectorAll('[data-accordion]');

            const head = Array.from(parts).find((part) => {
                return part.dataset.accordion === 'head';
            });
            const body = Array.from(parts).find((part) => {
                return part.dataset.accordion === 'body';
            });

            if (!head || !body) {
                return;
            }

            const closes = Array.from(parts).filter((part) => {
                return part.dataset.accordion === 'close';
            });

            if (closes.length > 0) {
                Array.from(closes).forEach((close) => {
                    close.addEventListener('click', (event) => {
                        const target = event.currentTarget;
                        const element = getElementFromFragment(target);
                        if (!element) {
                            return;
                        }

                        event.preventDefault();
                        event.stopImmediatePropagation();

                        element.classList.toggle('is-close');
                    });
                });
            }
        });
    }

    if (filtersTriggers.length > 0) {
        Array.from(filtersTriggers).forEach((trigger) => {
            if (!trigger.href || trigger.href.indexOf('#') === -1) {
                return;
            }

            const element = getElementFromFragment(trigger);
            if (!element) {
                return;
            }

            const list = element.querySelector('[data-filter]');

            trigger.addEventListener('click', (event) => {
                event.preventDefault();
                event.stopImmediatePropagation();

                const target = event.currentTarget;

                document.body.classList.toggle('is-filter-open');
                target.classList.toggle('is-open');
                list.classList.toggle('is-open');
            });

            if (list) {
                const selects = list.getElementsByTagName('select');

                if (selects.length > 0) {
                    Array.from(selects).forEach((select) => {
                        select.addEventListener('change', (event) => {
                            if (select.value) {
                                select.classList.add('has-value');
                            } else {
                                select.classList.remove('has-value');
                            }
                        });
                    });
                }
            }
        });
    }

    if (categories.length > 0) {
        Array.from(categories).forEach((category) => {
            const products = category.getElementsByClassName('js-product');

            if (products.length > 0) {
                Array.from(products).forEach((product) => {
                    product.addEventListener('click', (event) => {
                        const target = event.currentTarget;
                        const element = getElementFromFragment(target);
                        if (!element) {
                            return;
                        }

                        event.preventDefault();
                        event.stopImmediatePropagation();

                        const active = element.querySelector('.is-active');
                        if (active) {
                            active.classList.remove('is-active');
                        }

                        if (active !== target) {
                            target.classList.toggle('is-active');
                        }
                    });
                });
            }
        });
    }

    if (tabsHolders.length > 0) {
        Array.from(tabsHolders).forEach((tabsHolder) => {
            const tabs = tabsHolder.getElementsByTagName('a');

            if (tabs.length > 0) {
                Array.from(tabs).forEach((tab) => {
                    tab.addEventListener('click', (event) => {
                        event.preventDefault();
                        event.stopImmediatePropagation();

                        const target = event.currentTarget;
                        const parent = target.parentElement;
                        const active = tabsHolder.querySelector('.is-active');

                        if (parent !== active && target.dataset.image) {
                            changeThumbnail(target.dataset.image);
                        }

                        if (active) {
                            active.classList.remove('is-active');
                        }

                        if (parent.classList.contains('is-complete')) {
                            document.body.classList.add('is-complete-showing');
                        } else {
                            document.body.classList.remove('is-complete-showing');
                        }

                        if (isDesktop()) {
                            parent.classList.add('is-active');
                        } else {
                            if (parent === active) {
                                document.body.classList.remove('is-preview-open');
                                document.body.classList.remove('is-complete-showing');
                            } else {
                                parent.classList.add('is-active');
                                document.body.classList.add('is-preview-open');
                            }
                        }
                    });
                });
            }
        });
    }

  if (popupOpeners.length > 0) {
    Array.from(popupOpeners).forEach((popupOpener) => {
      popupOpener.addEventListener('click', (event) => {
        const target = event.currentTarget;
        const popup = getElementFromFragment(target);
        if (!popup) {
          return;
        }

        event.preventDefault();
        event.stopImmediatePropagation();

        popup.classList.toggle('is-open');
      });
    });
  }

    if (links.length) {
        for (const link of links) {
            link.addEventListener('click', (event) => {
                const target = event.currentTarget;
                const element = getElementFromFragment(target);
                if (!element) {
                    return;
                }

                event.preventDefault();

                const elementPosition = element.getBoundingClientRect().top;
                const offsetPosition = elementPosition + window.pageYOffset - anchors.offsetHeight;

                window.scrollTo({
                    top: Math.ceil(offsetPosition),
                    behavior: 'smooth'
                });
            });
        }
    }
    
    // */
});