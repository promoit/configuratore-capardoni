<?php

return [
    'header' => [
        'right' => 'To receive a quote on the current configuration or to request more information about other combinations or customizations, please reach out to us at',
        'category' => "Category",
        'page' => "Page",
        'pagination' => ":number of :count",
    ],
    'footer' => [
        'copy' => '©Copyright :year - CAPARDONI E C. SRL - PI IT00773460159',
        'disclaimer' => "The information declared in this catalogue are approximate and we reserve the right to make changes without prior notice.\nAdditional Items and combinations are available on request.",
    ],
    'content' => [
        'configuration' => "Configuration summary",
    ],
];
